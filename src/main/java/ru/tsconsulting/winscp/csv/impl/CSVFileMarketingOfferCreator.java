package ru.tsconsulting.winscp.csv.impl;

import com.opencsv.CSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.MappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import ru.tsconsulting.winscp.builder.entity.TRM01Entity;
import ru.tsconsulting.winscp.builder.entity.TRM06Entity;
import ru.tsconsulting.winscp.builder.logic.MarketingOfferBuilderManager;
import ru.tsconsulting.winscp.builder.logic.MarketingOfferGenerator;
import ru.tsconsulting.winscp.csv.CSVFileCreator;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * <h3>Класс, реализующий процесс создания файлов TRM01File.csv и TRM06File.csv, для формирования маркетингового
 * предложения</h3>
 * <p>Файл TRM01FIle.csv создается на основании класса-сущности {@link TRM01Entity}</p>
 * <p>Файл TRM06File.csv создается на основании класса-сущности {@link TRM06Entity}</p>
 *
 * @author ibetsis
 */
public final class CSVFileMarketingOfferCreator implements CSVFileCreator {
    private static final Logger LOGGER = Logger.getLogger(CSVFileMarketingOfferCreator.class);

    /**
     * <p>Название кампании в TRM</p>
     */
    private String companyName;

    /**
     * <p>Поле, соответсвующее количеству маркетинговых предложений для клиента</p>
     */
    private static int numberOfMarketingOffers;

    /**
     * <p>Метод, отвечающий за создание двух .csv файлов TRM01File.csv и TRM06File.csv, соответсвующих
     * маркетинговому предложению</p>
     * <p>Имена файлов генерируются с помощью {@link CSVFileMarketingOfferCreator#getFileName(String, String)}</p>
     *
     * @return - String массив содержащий имена файлов для маркетингового предложения
     *
     * @throws IllegalAccessException - ошибка доступа к полям класса
     */
    @Override
    public String[] createCSVFile() throws IllegalAccessException {
        MarketingOfferBuilderManager marketingOfferBuilderManager = new MarketingOfferBuilderManager();

        String fileNameTRM01 = getFileName(MarketingOfferGenerator.getClientId(), "1");
        String fileNameTRM06 = getFileName(MarketingOfferGenerator.getClientId(), "6");
        String[] fileTRMNames = {fileNameTRM01, fileNameTRM06};


        List<Pair<TRM01Entity, TRM06Entity>> pairs = new ArrayList<>();
        for (int i = 0; i < numberOfMarketingOffers; i++) {
            Pair<TRM01Entity, TRM06Entity> pair =
                    marketingOfferBuilderManager.constructorMarketingOffer();
            pairs.add(pair);
        }

        companyName = marketingOfferBuilderManager.getCompanyName();

        TRM01Entity[] trm01Entities = new TRM01Entity[numberOfMarketingOffers];
        TRM06Entity[] trm06Entities = new TRM06Entity[numberOfMarketingOffers];
        for (int i = 0; i < numberOfMarketingOffers; i++)
            trm01Entities[i] = pairs.get(i).getLeft();
        for (int j = 0; j < numberOfMarketingOffers; j++)
            trm06Entities[j] = pairs.get(j).getRight();
        createTRM01File(fileNameTRM01, trm01Entities);
        createTRM06File(fileNameTRM06, trm06Entities);

        return fileTRMNames;
    }

    /**
     * <p>Метод, отвечающий за создание двух .csv файлов TRM01File.csv и TRM06File.csv, соответсвующих
     * маркетинговому предложению с типом решения кредитного конвейера равным PACC</p>
     * <p>Имена файлов генерируются с помощью {@link CSVFileMarketingOfferCreator#getFileName(String, String)}</p>
     *
     * @return - String массив содержащий имена файлов для маркетингового предложения
     *
     * @throws IllegalAccessException - ошибка доступа к полям класса
     */
    public String[] createCSVFilePACC() throws IllegalAccessException {
        MarketingOfferBuilderManager marketingOfferBuilderManager = new MarketingOfferBuilderManager();

        String fileNameTRM01 = getFileName(MarketingOfferGenerator.getClientId(), "1");
        String fileNameTRM06 = getFileName(MarketingOfferGenerator.getClientId(), "6");
        String[] paccFileNames = {fileNameTRM01, fileNameTRM06};


        List<Pair<TRM01Entity, List<TRM06Entity>>> pairs = new ArrayList<>();
        for (int i = 0; i < numberOfMarketingOffers; i++) {
            Pair<TRM01Entity, List<TRM06Entity>> pair =
                    marketingOfferBuilderManager.constructorMarketingOfferPACC();
            pairs.add(pair);
        }

        companyName = marketingOfferBuilderManager.getCompanyName();

        TRM01Entity[] trm01Entities = new TRM01Entity[numberOfMarketingOffers];
        TRM06Entity[] trm06Entities = new TRM06Entity[6 * numberOfMarketingOffers];
        for (int i = 0; i < numberOfMarketingOffers; i++)
            trm01Entities[i] = pairs.get(i).getLeft();

        for (int i = 0; i < numberOfMarketingOffers; i++) {
            for (int j = i * 6, k = 0; j < 6 * (i + 1); j++, k++) {
                trm06Entities[j] = pairs.get(i).getRight().get(k);
            }
        }

        createTRM01File(fileNameTRM01, trm01Entities);
        createTRM06File(fileNameTRM06, trm06Entities);

        return paccFileNames;
    }

    /**
     * <p>Метод, отвечающий за создание двух .csv файлов TRM01File.csv и TRM06File.csv, соответсвующих
     * маркетинговому предложению спец вклада</p>
     * <p>Имена файлов генерируются с помощью {@link CSVFileMarketingOfferCreator#getFileName(String, String)}</p>
     *
     * @return - String массив содержащий имена файлов для маркетингового предложения
     *
     * @throws IllegalAccessException - ошибка доступа к полям класса
     */
    public String[] createCSVFileSpecDeposit() throws IllegalAccessException {
        MarketingOfferBuilderManager marketingOfferBuilderManager = new MarketingOfferBuilderManager();

        String fileNameTRM01 = getFileName(MarketingOfferGenerator.getClientId(), "1");
        String fileNameTRM06 = getFileName(MarketingOfferGenerator.getClientId(), "6");
        String[] paccFileNames = {fileNameTRM01, fileNameTRM06};


        List<Pair<TRM01Entity, List<TRM06Entity>>> pairs = new ArrayList<>();
        for (int i = 0; i < numberOfMarketingOffers; i++) {
            Pair<TRM01Entity, List<TRM06Entity>> pair =
                    marketingOfferBuilderManager.constructorMarketingOfferSpecDeposit();
            pairs.add(pair);
        }

        companyName = marketingOfferBuilderManager.getCompanyName();

        TRM01Entity[] trm01Entities = new TRM01Entity[numberOfMarketingOffers];
        TRM06Entity[] trm06Entities = new TRM06Entity[3 * numberOfMarketingOffers];
        for (int i = 0; i < numberOfMarketingOffers; i++)
            trm01Entities[i] = pairs.get(i).getLeft();

        for (int i = 0; i < numberOfMarketingOffers; i++) {
            for (int j = i * 3, k = 0; j < 3 * (i + 1); j++, k++) {
                trm06Entities[j] = pairs.get(i).getRight().get(k);
            }
        }

        createTRM01File(fileNameTRM01, trm01Entities);
        createTRM06File(fileNameTRM06, trm06Entities);

        return paccFileNames;
    }

    /**
     * <p>Метод, отвечающий за создание файла TRM01File.csv</p>
     * <p>Файл заполняется значениями из класса-сущности {@link TRM01Entity}. Разделителем в csv файле является
     * символ «VT» (Vertical Tab, ASCII 11). Кодировка файла: UTF-8</p>
     * <p>Для генерации файла используется библиотека {@link com.opencsv}. На основании java-бина, с помощью
     * {@link StatefulBeanToCsvBuilder#withMappingStrategy(MappingStrategy)} в создаваемый csv файл парсятся
     * значения, установленные стратегией маппинга (столбцы файла соответсвуют полям java-бина {@link TRM01Entity})</p>
     *
     * @param fileName  - имя генерируемого файла
     * @param TRM01File - массив объектов класса-сущности {@link TRM01Entity}
     */
    @SuppressWarnings("unchecked")
    private static void createTRM01File(String fileName, TRM01Entity... TRM01File) {
        try (CSVWriter writer = new CSVWriter(new OutputStreamWriter(new FileOutputStream(fileName), "UTF-8"),
                '\u000B', Character.MIN_VALUE, Character.MIN_VALUE, "\n")) {
            ColumnPositionMappingStrategy mappingStrategy = new ColumnPositionMappingStrategy();
            mappingStrategy.setType(TRM01Entity.class);

            String[] columns = TRM01File[0].getFieldNames();

            mappingStrategy.setColumnMapping(columns);

            StatefulBeanToCsvBuilder<TRM01Entity> builder = new StatefulBeanToCsvBuilder<>(writer);
            StatefulBeanToCsv beanWriter = builder.withMappingStrategy(mappingStrategy).build();

            for (TRM01Entity trm01Entity : TRM01File)
                beanWriter.write(trm01Entity);

            LOGGER.info("Created csv file: " + fileName);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * <p>Метод, отвечающий за создание файла TRM06File.csv</p>
     * <p>Файл заполняется значениями из класса-сущности {@link TRM06Entity}. Разделителем в csv файле является
     * символ «VT» (Vertical Tab, ASCII 11). Кодировка файла: UTF-8</p>
     * <p>Для генерации файла используется библиотека {@link com.opencsv}. На основании java-бина, с помощью
     * {@link StatefulBeanToCsvBuilder#withMappingStrategy(MappingStrategy)} в создаваемый csv файл парсятся
     * значения, установленные стратегией маппинга (столбцы файла соответсвуют полям java-бина {@link TRM06Entity})</p>
     *
     * @param fileName  - имя генерируемого файла
     * @param TRM06File - массив объектов класса-сущности {@link TRM06Entity}
     */
    @SuppressWarnings("unchecked")
    private static void createTRM06File(String fileName, TRM06Entity... TRM06File) {
        try (CSVWriter writer = new CSVWriter(new OutputStreamWriter(new FileOutputStream(fileName), "UTF-8"),
                '\u000B', Character.MIN_VALUE, Character.MIN_VALUE, "\n")) {
            ColumnPositionMappingStrategy mappingStrategy = new ColumnPositionMappingStrategy();
            mappingStrategy.setType(TRM06Entity.class);

            String[] columns = TRM06File[0].getFieldsName();
            mappingStrategy.setColumnMapping(columns);

            StatefulBeanToCsvBuilder<TRM01Entity> builder = new StatefulBeanToCsvBuilder<>(writer);
            StatefulBeanToCsv beanWriter = builder.withMappingStrategy(mappingStrategy).build();

            for (TRM06Entity trm06Entity : TRM06File)
                beanWriter.write(trm06Entity);

            LOGGER.info("Created csv file: " + fileName);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * <p>Метод удаляет файлы с именами {@param fileNameTRM01}, {@param fileNameTRM06}
     * из локальной корневой папки проекта</p>
     * <p>Файл с именем {@param fileNameTRM01} построен на основании сущности {@link TRM01Entity}</p>
     * <p>Файл с именем {@param fileNameTRM06} построен на основании сущности {@link TRM06Entity}</p>
     *
     * @param fileNameTRM01 - имя файла с точкой интеграции TRM01
     * @param fileNameTRM06 - имя файла с точкой инетграции TRM06
     */
    public void deleteLocalFileMarketingOffer(String fileNameTRM01, String fileNameTRM06) {
        File TRM01File = new File(fileNameTRM01);
        File TRM06File = new File(fileNameTRM06);

        TRM01File.delete();
        TRM06File.delete();

        LOGGER.info("Delete files on local: " + fileNameTRM01 + ", " + fileNameTRM06);
    }

    /**
     * <p>Метод, формирующий название файла для загрузки в Siebel</p>
     *
     * @param clientId      - id клиента в Siebel
     * @param numberTRMFile - номер точки интеграции TRM
     * @return - имя файла
     */
    private static String getFileName(String clientId, String numberTRMFile) {
        String offerType = MarketingOfferGenerator.getTypeDecisionCreditConveyor();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYYMMddhhmmss");
        Calendar calendar = GregorianCalendar.getInstance();
        return "TRM0" + numberTRMFile + "_" + offerType + "_" +
                simpleDateFormat.format(calendar.getTime()) + ".csv";
    }

    public String getCompanyName() {
        return companyName;
    }

    public static int getNumberOfMarketingOffers() {
        return numberOfMarketingOffers;
    }

    public static void setNumberOfMarketingOffers(int numberOfMarketingOffers) {
        CSVFileMarketingOfferCreator.numberOfMarketingOffers = numberOfMarketingOffers;
    }
}
