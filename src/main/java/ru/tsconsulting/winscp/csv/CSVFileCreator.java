package ru.tsconsulting.winscp.csv;

/**
 * <h3>Интерфейс, определяющий создание файла с расширением .csv</h3>
 *
 * @author ibetsis
 */
public interface CSVFileCreator {

    /**
     * <p>Метод создающий файл с расширением csv</p>
     *
     * @return - String массив содержащий имена созданных файлов
     * @throws IllegalAccessException
     */
    String[] createCSVFile() throws IllegalAccessException;
}
