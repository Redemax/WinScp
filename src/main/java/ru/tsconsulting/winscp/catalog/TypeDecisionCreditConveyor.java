package ru.tsconsulting.winscp.catalog;

/**
 * <h3>Перечисление, соответсвующие типам решений КК</h3>
 * <p>Значения берутся из справочника "Типы решений КК" (стандартный справочник в ЕФР с кодом VTB24_CONCL_TYPE )</p>
 * <p>Пустное значение {@param SPECIAL_DEPOSIT} соответствует типу кампаний для спец. вклада</p>
 *
 * @author ibetsis
 */
public enum TypeDecisionCreditConveyor implements MarketingOption {
    PACL("PACL"),
    TOP_UP("TOPUP"),
    PACC("PACC"),
    PACL_PACC("PACL + PACC"),
    TOPUP_PACC("TOPUP + PACC"),
    SPECIAL_DEPOSIT("specdep");

    private String name;

    TypeDecisionCreditConveyor(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
