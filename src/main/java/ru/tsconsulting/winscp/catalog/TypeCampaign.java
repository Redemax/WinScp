package ru.tsconsulting.winscp.catalog;

/**
 * <h3>Перечисление, соответсвующее типу кампании</h3>
 *
 * @author ibetsis
 */
public enum TypeCampaign implements MarketingOption{
    PACC("PACC"),
    PACL("PACL"),
    TOP_UP("TOPUP"),
    COMBO("COMBO"),
    SPECIAL_DEPOSIT("Спец. вклады");

    private String typeCampaign;

    TypeCampaign(String typeCampaign) {
        this.typeCampaign = typeCampaign;
    }

    public String getTypeCampaign() {
        return typeCampaign;
    }
}
