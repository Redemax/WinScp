package ru.tsconsulting.winscp.catalog;

/**
 * <h3>Перечисление, соответствующее каналу обращения для маркетингового предложения</h3>
 *
 * @author ibetsis
 */
public enum MarketingOfferChannel implements MarketingOption {
    EMAIL("Email"),
    INCOMING_CALL("Входящее обращение"),
    CC_ENTRANCE("КЦ-Вход"),
    TELEMARKETING("Телемаркетинг");

    private String encodedValue;

    MarketingOfferChannel(String encodedValue) {
        this.encodedValue = encodedValue;
    }

    @Override
    public String toString() {
        return encodedValue;
    }
}
