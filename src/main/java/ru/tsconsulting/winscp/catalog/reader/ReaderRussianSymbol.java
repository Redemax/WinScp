package ru.tsconsulting.winscp.catalog.reader;

import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;

/**
 * <h3>Класс, педназначенный для корректного преобразования русских символов,
 *  для записи в csv файл</h3>
 *  <p>Не рекомендуем к использованию. Генерируемый файлы уже в кодировке UTF-8</p>
 *
 * @author ibetsis
 */
@Deprecated
public class ReaderRussianSymbol {
    private static final Logger LOGGER = Logger.getLogger(ReaderRussianSymbol.class);

    public static String read(String str) {
        try {
            return new String(str.getBytes("UTF-8"), "Cp1251");
        } catch (UnsupportedEncodingException e) {
            LOGGER.error(e.getMessage(),e);
        }
        return null;
    }
}
