package ru.tsconsulting.winscp.catalog;

/**
 * <h3>Перечисление, соответсвующее филиалу бисквит для маркетингового предложения</h3>
 *
 * @author ibetsis
 */
public enum BisquitBranch implements MarketingOption {
    /**
     * <p>Значение, соответствующее головному офису</p>
     */
    HEAD_OFFICE("00000", "БИСКвит: головной офис"),

    /**
     * <p>Значение офиса для спец вкладов</p>
     */
    OFFICE_FOR_SPECIAL_DEPOSIT("00000","");

    private String bisquitId;
    private String bisquitName;

    BisquitBranch(String bisquitId, String bisquitName) {
        this.bisquitId = bisquitId;
        this.bisquitName = bisquitName;
    }

    public String getBisquitId() {
        return bisquitId;
    }

    public String getBisquitName() {
        return bisquitName;
    }
}
