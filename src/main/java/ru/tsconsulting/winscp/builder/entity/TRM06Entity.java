package ru.tsconsulting.winscp.builder.entity;

import ru.tsconsulting.winscp.builder.logic.TRM06Builder;

import java.lang.reflect.Field;

/**
 * <h3>Класс-сущность соответсвующая файлу .csv, отвечающему за экспорт матрицы решений КК</h3>
 *  <p>Является частью бизнес процесса маркетинговго предложения</p>
 *
 * @see TRM06Builder - класс строитель данной сущности
 * @see ru.tsconsulting.winscp.builder.logic.MarketingOfferBuilderManager - класс строитель для сущности маркетингового
 * предложения. Маркетинговое предложения включает в себя классы-сущности  {@link TRM01Entity} и {@link TRM06Entity}
 *
 * @author ibetsis
 */
public final class TRM06Entity implements CSVEntity,WinSCPEntity {
    /**
     * <p>Поле, соответсвующее номеру маркетингового предложения</p>
     * <p>Бизнес-логика маркетингового предложния требует, чтобы оно было уникальным</p>
     * <p>Генерируется с помощью {@link ru.tsconsulting.winscp.builder.logic.MarketingOfferGenerator}</p>
     */
    private String id;

    /**
     * <p>Уникальный номер решения КК. Уникальность обеспечивается в рамках матрицы для одного участника</p>
     */
    private String numberDecisionCreditConveyor;

    /**
     * <p>Минимальная предодобренная сумма</p>
     */
    private String minPrice;

    /**
     * <p>Максимальная предодобренная сумма</p>
     */
    private String maxPrice;

    /**
     * <p>Одобренная процентная ставка для PAAL, PACL, PACC и остальных кредитов</p>
     */
    private String interestRate;

    /**
     * <p>Срок кредита, одобренный срок по заявкам на PAAL, PACL, PACC и срок вклада</p>
     */
    private String periodOffer;

    /**
     * <p>Номер договора для погашения</p>
     */
    private String numberOffer;

    /**
     * <p>Система ведения погашаемого кредита (для кампаний типа TOPUP)</p>
     */
    private String systemLeadCredit;

    /**
     * <p>Ежемесячный платеж</p>
     */
    private String monthlyPayment;

    /**
     * <p>Признак основного решения КК (в разрезе «Типа решения» для участника)</p>
     *
     * <p>Y – является основным решением</p>
     *
     * <p>Заполняется для каждого типа решений (кроме PACC)</p>
     */
    private String signMainDecisionCreditConveyor;

    /**
     * <p>Код филиала погашаемого договора</p>
     */
    private String codeBranch;

    /**
     * <p>Тип контракта в системе-источнике</p>
     */
    private String typeContract;

    /**
     * <p>Код субпродукта (необходимо заполнять кодом субпродукта маркетингового предложения</p>
     * <p>Поле обязательно для кампаний типа TOPUP/PACL/PACC</p>
     *
     * <p>Для Релиза 1.5 не заполняется</p>
     */
    private String subproductCode;

    /**
     * <p>Тип решения (заполняется для кампаний Релиза 4)</p>
     */
    private String typeDecision;

    /**
     * <p>Краткое описание продукта в рамках решения, необходимое для
     * озвучивания Оператором КЦ / Сотрудником ТП</p>
     * <p>(заполняется только для Релиза 4)</p>
     */
    private String shortInfoAboutProduct;

    /**
     * <p>Наименование спец. вклада
     * Обязательный для спец. вкладов</p>
     */
    private String depositName;

    /**
     * <p>Тип спец. вклада
     * Обязательный для спец. вкладов</p>
     */
    private String depositType;

    /**
     * <p>Минимальный срок вклада
     * Обязательный для спец. вкладов </p>
     */
    private String minTermDeposit;

    /**
     * <p>Максимальный срок вклада
     * Обязательный для спец. вкладов</p>
     */
    private String maxTermDeposit;

    /**
     * <p>Минимальный остаток
     * Обязательный для спец. вкладов</p>
     */
    private String depositBalanceIrreducible;

    /**
     * <p>Способ выплаты %%
     * Обязательный для спец. вкладов</p>
     */
    private String depositPaymentType;

    /**
     * <p>Код процентной ставки
     * Обязательный для спец. вкладов</p>
     */
    private String commisionCode;

    /**
     * <p>Валюта вклада
     * Обязательный для спец. вкладов</p>
     */
    private String depositCurrency;

    /**
     * <p>Возможность частичного списания</p>
     */
    private String decreaseFlag;

    /**
     * <p>Возможность пополнения</p>
     */
    private String increaseFlag;

    /**
     * <p>Конструктор класса-сущности {@link TRM06Entity}</p>
     *
     * @param TRM06FileValues - String массив, соответсвующий маркетинговому предложению. Заполняется с помощью
     *                          {@link ru.tsconsulting.winscp.builder.logic.MarketingOfferGenerator}
     * @throws IllegalAccessException - ошибка доступа к полям класса
     */
    public TRM06Entity(String[] TRM06FileValues) throws IllegalAccessException{
        Field[] f = getClass().getDeclaredFields();

        for (int i = 0; i < f.length; i++) {
            f[i].setAccessible(true);
            f[i].set(this, TRM06FileValues[i]);
        }
    }

    /**
     * <p>Метод получает массив имен полей класса</p>
     *
     * @return String массив, содержащий все имена полей класса
     */
    public String[] getFieldsName() {
        Field[] fields = getClass().getDeclaredFields();
        String[] fieldsName = new String[fields.length];

        for (int i = 0; i < fields.length; i++) {
            fieldsName[i] = fields[i].getName();
        }
        return fieldsName;
    }

    public String getId() {
        return id;
    }
    public String getNumberDecisionCreditConveyor() {
        return numberDecisionCreditConveyor;
    }
    public String getMinPrice() {
        return minPrice;
    }
    public String getMaxPrice() {
        return maxPrice;
    }
    public String getInterestRate() {
        return interestRate;
    }
    public String getPeriodOffer() {
        return periodOffer;
    }
    public String getNumberOffer() {
        return numberOffer;
    }
    public String getSystemLeadCredit() {
        return systemLeadCredit;
    }
    public String getMonthlyPayment() {
        return monthlyPayment;
    }
    public String getSignMainDecisionCreditConveyor() {
        return signMainDecisionCreditConveyor;
    }
    public String getCodeBranch() {
        return codeBranch;
    }
    public String getTypeContract() {
        return typeContract;
    }
    public String getSubproductCode() {
        return subproductCode;
    }
    public String getTypeDecision() {
        return typeDecision;
    }
    public String getShortInfoAboutProduct() {
        return shortInfoAboutProduct;
    }
    public String getDepositName() { return depositName; }
    public String getDepositType() { return depositType; }
    public String getMinTermDeposit() { return minTermDeposit; }
    public String getMaxTermDeposit() { return maxTermDeposit; }
    public String getDepositBalanceIrreducible() { return depositBalanceIrreducible; }
    public String getDepositPaymentType() { return depositPaymentType; }
    public String getCommisionCode() { return commisionCode; }
    public String getDepositCurrency() { return depositCurrency; }
    public String getDecreaseFlag() { return decreaseFlag; }
    public String getIncreaseFlag() { return increaseFlag; }
}
