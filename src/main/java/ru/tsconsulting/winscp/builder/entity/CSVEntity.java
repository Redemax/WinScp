package ru.tsconsulting.winscp.builder.entity;

/**
 * <h3>Интерфейс, определяющий класс-сущность соответсвующую файлу с расширенеем .csv</h3>
 *
 * @author ibetsis
 */
interface CSVEntity extends WinSCPEntity {

}
