package ru.tsconsulting.winscp.builder.entity;

import ru.tsconsulting.winscp.builder.logic.TRM01Builder;

import java.lang.reflect.Field;

/**
 * <h3>Класс-сущность соответсвующая файлу .csv, отвечающему за загрузку данных по участникам кампаний ФЛ
 *     из системы TRM в ФРОНТ</k3>
 *  <p>Является частью бизнес процесса маркетингового предложения</p>
 *
 * @see TRM01Builder - класс строитель данной сущности
 * @see ru.tsconsulting.winscp.builder.logic.MarketingOfferBuilderManager - класс строитель для сущности маркетингового
 * предложения. Маркетинговое предложение включает в себя классы-сущности  {@link TRM01Entity} и {@link TRM06Entity}
 *
 * @author ibetsis
 */
public final class TRM01Entity implements CSVEntity,WinSCPEntity {
    /**
     * <p>Поле, соответсвующее номеру маркетингового предложения</p>
     * <p>Бизнес-логика маркетингового предложния требует, чтобы оно было уникальным</p>
     * <p>Генерируется с помощью {@link ru.tsconsulting.winscp.builder.logic.MarketingOfferGenerator}</p>
     */
    private String id;

    /**
     * <p>Название кампании в TRM</p>
     */
    private String companyName;

    /**
     * <p>Код кампании в TRM</p>
     */
    private String companyCode;

    /**
     * <p>Тип кампании</p>
     */
    private String marketingOfferType;

    /**
     * <p>Номер предложения</p>
     */
    private String offerNumber;

    /**
     * <p>Название предложения</p>
     */
    private String offerName;

    /**
     * <p>Приоритет предложения</p>
     */
    private String offerPrior;

    /**
     * <p>Канал, по которому поступает маркетинговое предложение</p>
     */
    private String channel;

    /**
     * <p>ГРК (хз)</p>
     */
    private String grk;

    /**
     * <p>Идентификатор клиента в системе Siebel</p>
     */
    private String clientId;

    /**
     * <p>Id БИСквита</p>
     */
    private String biskvitId;

    /**
     * <p>БИСквит (система-источник)</p>
     */
    private String biskvitName;

    /**
     * <p>Номер предложения в БИСквите</p>
     */
    private String offerNumberInBC;

    /**
     * <p>Временная зона</p>
     */
    private String timeZone;

    /**
     * <p>Флаг удаления из списка (значения: 0/1)</p>
     */
    private String deleteFlag;

    /**
     * <p>Информация о маркетинговом предложении для клиента</p>
     */
    private String offerInfo;

    /**
     * <p>Дата выдачи продукта участнику кампании</p>
     */
    private String dateOutput;

    /**
     * <p>Дата заведения заявки на продукт</p>
     */
    private String startDate;

    /**
     * <p>Статус заявки на кредитный продукт (заявки с сайта, на КК и КН)</p>
     */
    private String offerStatus;

    /**
     * <p>Срок истечения предложения
     * (для TOPUP/PACL должно содержать дату истечения предодобренного предложения)</p>
     */
    private String endDate;

    /**
     * <p>Одобренная процентная ставка для PAAL, PACL, и остальных кредитов
     * (для TOPUP/PACL процентная ставка основного решения КК из матрицы решений предложения)</p>
     *
     * <p>Для Релиза 4 не заполняется</p>
     */
    private String interestRate;

    /**
     *  <p>Сумма выданного кредита, одобренный лимит по заявкам на PAAL, PACL, и сумма вклада
     *  для депозитов (для TOPUP/PACL сумма основного решения КК из матрицы решений предложения</p>
     *
     *  <p>Для Релиза 4 не заполняется</p>
     */
    private String sumOutputCredit;

    /**
     * <p>Срок кредита, одобренный срок по заявкам на PAAL, PACL, и срок вклада для депозитов
     * (для TOPUP/PACL срок основного решения КК из матрицы решений предложения)</p>
     *
     * <p>Для Релиза 4 не заполняется</p>
     */
    private String timeOutputCredit;

    /**
     * <p>Валюта, в которой предлагается продукт (для TOPUP/PACL/PACC код валюты предложения)</p>
     */
    private String currency;

    /**
     * <p>Плановая дата закрытия вклада</p>
     */
    private String endDatePlaning;

    /**
     * <p>Номер отделения, в котором была оформлена заявка</p>
     */
    private String offerDepartmentNumber;

    /**
     * <p>Наименование переменного поля 1</p>
     */
    private String nameVar1;

    /**
     * <p>Значение переменного поля 1</p>
     */
    private String valueVar1;

    /**
     * <p>Наименование переменного поля 2</p>
     */
    private String nameVar2;

    /**
     * <p>Значение переменного поля 2</p>
     */
    private String valueVar2;

    /**
     * <p>Наименование переменного поля 3</p>
     */
    private String nameVar3;

    /**
     * <p>Значение переменного поля 3</p>
     */
    private String valueVar3;

    /**
     * <p>Наименование переменного поля 4</p>
     */
    private String nameVar4;

    /**
     * <p>Значение переменного поля 4</p>
     */
    private String valueVar4;

    /**
     * <p>Наименование переменного поля 5</p>
     */
    private String nameVar5;

    /**
     * <p>Значение переменного поля 5</p>
     */
    private String valueVar5;

    /**
     * <p>Наименование переменного поля 6</p>
     */
    private String nameVar6;

    /**
     * <p>Значение переменного поля 6</p>
     */
    private String valueVar6;

    /**
     * <p>Наименование переменного поля 7</p>
     */
    private String nameVar7;

    /**
     * <p>Значение переменного поля 7</p>
     */
    private String valueVar7;

    /**
     * <p>Наименование переменного поля 8</p>
     */
    private String nameVar8;

    /**
     * <p>Значение переменного поля 8</p>
     */
    private String valueVar8;

    /**
     * <p>Наименование переменного поля 9</p>
     */
    private String nameVar9;

    /**
     * <p>Значение переменного поля 9</p>
     */
    private String valueVar9;

    /**
     * <p>Наименование переменного поля 10</p>
     */
    private String nameVar10;

    /**
     * <p>Значение переменного поля 10</p>
     */
    private String valueVar10;

    /**
     * <p>Код субпродукта (необходимо заполнять кодом субпродукта маркетингового предложения,
     * существующий параметр PRODUCT заполнять соответствующим кодом продукта</p>
     *
     * <p>поле обязательно для кампаний типа TOPUP/PACL</p>
     *
     * <p>Для Релиза 4 не заполняется</p>
     */
    private String subProduct;

    /**
     * <p>Код продукта</p>
     * <p>Для Релиза 4 не заполняется</p>
     */
    private String product;

    /**
     * <p>Флаг, указывающий на наличие матрицы решений КК для данного предложения (флаг (Y/N)
     * отражающий наличие дочерних записей матрицы решений КК в отдельном файле)</p>
     */
    private String conclusionCreditConveyor;

    /**
     * <p>Признак необходимости создать задачу, связанную с карточкой  участника МП</p>
     *
     * <p>Y – необходимо создавать задачу</p>
     */
    private String needCreateTask;

    /**
     * <p>Дата завершения план по задаче</p>
     */
    private String expireDateTask;

    /**
     * <p>Тип задачи. Обязательно в случае указания подтипа задачи</p>
     */
    private String typeTask;

    /**
     * <p>Подтип задачи. При указании подтипа задачи обязательно должен быть указан тип задачи</p>
     */
    private String subtypeTask;

    /**
     * <p>Код бисквита ТП супервизора</p>
     */
    private String codeTPSupervision;

    /**
     * <p>Приоритетный тип решения для МП (заполняется для кампаний Релиза 4)</p>
     */
    private String priorTypeForOffer;

    /**
     * <p>Признак необходимости установки отклика на МП</p>
     *
     * <p>Y – необходимо установить отклик.
     * В случае передачи пустого значения по умолчанию устанавливается значение N</p>
     */
    private String needResponse;

    /**
     * <p>Конструктор класса-сущности {@link TRM01Entity}</p>
     *
     * @param TRM01FileValues - String массив, соответствующий маркетинговому предложению. Заполняется с помощью
     *                          {@link ru.tsconsulting.winscp.builder.logic.MarketingOfferGenerator}
     * @throws IllegalAccessException - ошибка доступа к полям класса
     */
    public TRM01Entity(String[] TRM01FileValues) throws IllegalAccessException{
        Field[] f = getClass().getDeclaredFields();

        for (int i = 0; i < f.length; i++) {
            f[i].setAccessible(true);
            f[i].set(this, TRM01FileValues[i]);
        }
    }

    /**
     * <p>Метод получает массив имен полей класса</p>
     *
     * @return String массив, содержащий все имена полей класса
     */
    public String[] getFieldNames() {
        Field[] fields = getClass().getDeclaredFields();
        String[] fieldsName = new String[fields.length];

        for (int i = 0; i < fields.length; i++) {
            fieldsName[i] = fields[i].getName();
        }

        return fieldsName;
    }

    public String getId() {
        return id;
    }
    public String getCompanyName() {
        return companyName;
    }
    public String getCompanyCode() {
        return companyCode;
    }
    public String getMarketingOfferType() {
        return marketingOfferType;
    }
    public String getOfferNumber() {
        return offerNumber;
    }
    public String getOfferName() {
        return offerName;
    }
    public String getOfferPrior() {
        return offerPrior;
    }
    public String getChannel() {
        return channel;
    }
    public String getGrk() {
        return grk;
    }
    public String getClientId() {
        return clientId;
    }
    public String getBiskvitId() {
        return biskvitId;
    }
    public String getBiskvitName() {
        return biskvitName;
    }
    public String getOfferNumberInBC() {
        return offerNumberInBC;
    }
    public String getTimeZone() {
        return timeZone;
    }
    public String getDeleteFlag() {
        return deleteFlag;
    }
    public String getOfferInfo() {
        return offerInfo;
    }
    public String getDateOutput() {
        return dateOutput;
    }
    public String getStartDate() {
        return startDate;
    }
    public String getOfferStatus() {
        return offerStatus;
    }
    public String getEndDate() {
        return endDate;
    }
    public String getInterestRate() {
        return interestRate;
    }
    public String getSumOutputCredit() {
        return sumOutputCredit;
    }
    public String getTimeOutputCredit() {
        return timeOutputCredit;
    }
    public String getCurrency() {
        return currency;
    }
    public String getEndDatePlaning() {
        return endDatePlaning;
    }
    public String getOfferDepartmentNumber() {
        return offerDepartmentNumber;
    }
    public String getNameVar1() {
        return nameVar1;
    }
    public String getValueVar1() {
        return valueVar1;
    }
    public String getNameVar2() {
        return nameVar2;
    }
    public String getValueVar2() {
        return valueVar2;
    }
    public String getNameVar3() {
        return nameVar3;
    }
    public String getValueVar3() {
        return valueVar3;
    }
    public String getNameVar4() {
        return nameVar4;
    }
    public String getValueVar4() {
        return valueVar4;
    }
    public String getNameVar5() {
        return nameVar5;
    }
    public String getValueVar5() {
        return valueVar5;
    }
    public String getNameVar6() {
        return nameVar6;
    }
    public String getValueVar6() {
        return valueVar6;
    }
    public String getNameVar7() {
        return nameVar7;
    }
    public String getValueVar7() {
        return valueVar7;
    }
    public String getNameVar8() {
        return nameVar8;
    }
    public String getValueVar8() {
        return valueVar8;
    }
    public String getNameVar9() {
        return nameVar9;
    }
    public String getValueVar9() {
        return valueVar9;
    }
    public String getNameVar10() {
        return nameVar10;
    }
    public String getValueVar10() {
        return valueVar10;
    }
    public String getSubProduct() {
        return subProduct;
    }
    public String getProduct() {
        return product;
    }
    public String getConclusionCreditConveyor() {
        return conclusionCreditConveyor;
    }
    public String getNeedCreateTask() {
        return needCreateTask;
    }
    public String getExpireDateTask() {
        return expireDateTask;
    }
    public String getTypeTask() {
        return typeTask;
    }
    public String getSubtypeTask() {
        return subtypeTask;
    }
    public String getCodeTPSupervision() {
        return codeTPSupervision;
    }
    public String getPriorTypeForOffer() {
        return priorTypeForOffer;
    }
    public String getNeedResponse() {
        return needResponse;
    }
}
