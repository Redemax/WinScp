package ru.tsconsulting.winscp.builder.logic;

import ru.tsconsulting.winscp.builder.entity.TRM06Entity;

/**
 * <h3>Строитель класса {@link TRM06Entity}</h3>
 *
 * @author ibetsis
 */
public final class TRM06Builder {
    /**
     * <p>Поле, соответсвующее номеру маркетингового предложения</p>
     * <p>Бизнес-логика маркетингового предложния требует, чтобы оно было уникальным</p>
     * <p>Генерируется с помощью {@link ru.tsconsulting.winscp.builder.logic.MarketingOfferGenerator}</p>
     */
    private String id;

    /**
     * <p>Уникальный номер решения КК. Уникальность обеспечивается в рамках матрицы для одного участника</p>
     */
    private String numberDecisionCreditConveyor;

    /**
     * <p>Минимальная предодобренная сумма</p>
     */
    private String minPrice;

    /**
     * <p>Максимальная предодобренная сумма</p>
     */
    private String maxPrice;

    /**
     * <p>Одобренная процентная ставка для PAAL, PACL, PACC и остальных кредитов</p>
     */
    private String interestRate;

    /**
     * <p>Срок кредита, одобренный срок по заявкам на PAAL, PACL, PACC и срок вклада</p>
     */
    private String periodOffer;

    /**
     * <p>Номер договора для погашения</p>
     */
    private String numberOffer;

    /**
     * <p>Система ведения погашаемого кредита (для кампаний типа TOPUP)</p>
     */
    private String systemLeadCredit;

    /**
     * <p>Ежемесячный платеж</p>
     */
    private String monthlyPayment;

    /**
     * <p>Признак основного решения КК (в разрезе «Типа решения» для участника)</p>
     *
     * <p>Y – является основным решением</p>
     *
     * <p>Заполняется для каждого типа решений (кроме PACC)</p>
     */
    private String signMainDecisionCreditConveyor;

    /**
     * <p>Код филиала погашаемого договора</p>
     */
    private String codeBranch;

    /**
     * <p>Тип контракта в системе-источнике</p>
     */
    private String typeContract;

    /**
     * <p>Код субпродукта (необходимо заполнять кодом субпродукта маркетингового предложения</p>
     * <p>Поле обязательно для кампаний типа TOPUP/PACL/PACC</p>
     *
     * <p>Для Релиза 1.5 не заполняется</p>
     */
    private String subproductCode;

    /**
     * <p>Тип решения (заполняется для кампаний Релиза 4)</p>
     */
    private String typeDecision;

    /**
     * <p>Краткое описание продукта в рамках решения, необходимое для
     * озвучивания Оператором КЦ / Сотрудником ТП</p>
     * <p>(заполняется только для Релиза 4)</p>
     */
    private String shortInfoAboutProduct;

    /**
     * <p>Наименование спец. вклада
     * Обязательный для спец. вкладов</p>
     */
    private String depositName;

    /**
     * <p>Тип спец. вклада
     * Обязательный для спец. вкладов</p>
     */
    private String depositType;

    /**
     * <p>Минимальный срок вклада
     * Обязательный для спец. вкладов </p>
     */
    private String minTermDeposit;

    /**
     * <p>Максимальный срок вклада
     * Обязательный для спец. вкладов</p>
     */
    private String maxTermDeposit;

    /**
     * <p>Минимальный остаток
     * Обязательный для спец. вкладов</p>
     */
    private String depositBalanceIrreducible;

    /**
     * <p>Способ выплаты %%
     * Обязательный для спец. вкладов</p>
     */
    private String depositPaymentType;

    /**
     * <p>Код процентной ставки
     * Обязательный для спец. вкладов</p>
     */
    private String commisionCode;

    /**
     * <p>Валюта вклада
     * Обязательный для спец. вкладов</p>
     */
    private String depositCurrency;

    /**
     * <p>Возможность частичного списания</p>
     */
    private String decreaseFlag;

    /**
     * <p>Возможность пополнения</p>
     */
    private String increaseFlag;

    /**
     * <p>Метод, создающий объект класса-сущности {@link TRM06Entity}, на основании значений полученных с помощью
     *  генератора {@link MarketingOfferGenerator} </p>
     *
     * @param TRM06FileValues - String массив, соответсвующий маркетинговому предложению. Заполняется с помощью
     *                          {@link ru.tsconsulting.winscp.builder.logic.TRM06ValuesGenerator}
     * @return - объект класса {@link TRM06Entity}
     * @throws IllegalAccessException - ошибка доступа к полям класса
     */
    TRM06Entity getTRM06(String[] TRM06FileValues) throws IllegalAccessException{
        return new TRM06Entity(TRM06FileValues);
    }

    public void setId(String id) {
        this.id = id;
    }
    public void setNumberDecisionCreditConveyor(String numberDecisionCreditConveyor) {
        this.numberDecisionCreditConveyor = numberDecisionCreditConveyor;
    }
    public void setMinPrice(String minPrice) {
        this.minPrice = minPrice;
    }
    public void setMaxPrice(String maxPrice) {
        this.maxPrice = maxPrice;
    }
    public void setInterestRate(String interestRate) {
        this.interestRate = interestRate;
    }
    public void setPeriodOffer(String periodOffer) {
        this.periodOffer = periodOffer;
    }
    public void setNumberOffer(String numberOffer) {
        this.numberOffer = numberOffer;
    }
    public void setSystemLeadCredit(String systemLeadCredit) {
        this.systemLeadCredit = systemLeadCredit;
    }
    public void setMonthlyPayment(String monthlyPayment) {
        this.monthlyPayment = monthlyPayment;
    }
    public void setSignMainDecisionCreditConveyor(String signMainDecisionCreditConveyor) {
        this.signMainDecisionCreditConveyor = signMainDecisionCreditConveyor;
    }
    public void setCodeBranch(String codeBranch) {
        this.codeBranch = codeBranch;
    }
    public void setTypeContract(String typeContract) {
        this.typeContract = typeContract;
    }
    public void setSubproductCode(String subproductCode) {
        this.subproductCode = subproductCode;
    }
    public void setTypeDecision(String typeDecision) {
        this.typeDecision = typeDecision;
    }
    public void setShortInfoAboutProduct(String shortInfoAboutProduct) {
        this.shortInfoAboutProduct = shortInfoAboutProduct;
    }
    public void setDepositName(String depositName) { this.depositName = depositName; }
    public void setDepositType(String depositType) { this.depositType = depositType; }
    public void setMinTermDeposit(String minTermDeposit) { this.minTermDeposit = minTermDeposit; }
    public void setMaxTermDeposit(String maxTermDeposit) { this.maxTermDeposit = maxTermDeposit; }
    public void setDepositBalanceIrreducible(String depositBalanceIrreducible) {
        this.depositBalanceIrreducible = depositBalanceIrreducible;
    }
    public void setDepositPaymentType(String depositPaymentType) { this.depositPaymentType = depositPaymentType; }
    public void setCommisionCode(String commisionCode) { this.commisionCode = commisionCode; }
    public void setDepositCurrency(String depositCurrency) { this.depositCurrency = depositCurrency; }
    public void setDecreaseFlag(String decreaseFlag) { this.decreaseFlag = decreaseFlag; }
    public void setIncreaseFlag(String increaseFlag) { this.increaseFlag = increaseFlag; }
}
