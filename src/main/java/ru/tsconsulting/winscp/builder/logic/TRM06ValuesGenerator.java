package ru.tsconsulting.winscp.builder.logic;

import ru.tsconsulting.winscp.builder.entity.TRM06Entity;
import ru.tsconsulting.winscp.catalog.TypeDecisionCreditConveyor;

import java.util.ArrayList;
import java.util.List;

/**
 * <h3>Класс-генератор, соответствующий генерации значений для заполнения сущности {@link TRM06Entity}</h3>
 * <p>Основные опции для маркетингового предложения устанавливаются в {@link MarketingOfferGenerator}</p>
 *
 * @author ibetsis
 */
final class TRM06ValuesGenerator extends MarketingOfferGenerator {

    /**
     * <p>Метод, генерирующий массив значений маркетингового предложения для класса-сущности
     * {@link TRM06Entity}</p>
     *
     * @return String массив значений для класса сущности {@link TRM06Entity}
     */
    String[] TRM06FileValues() {
        return getTRM06FileValue();
    }

    /**
     * <p>Метод, генерирующий список массивов значений маркетингового предложения для класса-сущности
     * {@link TRM06Entity} с типом решения КК равным PACC</p>
     *
     * <p>Содержит шесть типов решения КК</p>
     *
     * @return String массив значений для класса сущности {@link TRM06Entity}
     */
    List<String[]> TRM06FileValuesPACC() {
        return getTRM06FileValuesPACC();
    }

    /**
     * <p>Метод, генерирующий список массивов значений маркетингового предложения для класса-сущности
     * {@link TRM06Entity} ооответствующий спец. вкладу</p>
     *
     * <p>Содержит три типа решения КК</p>
     *
     * @return String массив значений для класса сущности {@link TRM06Entity}
     */
    List<String[]> TRM06FileValuesSpecDeporsit() {
        return getTRM06FileValuesSpecDeposit();
    }

    /**
     * <p>Метод осуществляет маппинг справочных значений и генерирует массив входных данных для файла,
     * соответствующего сущности {@link TRM06Entity} маркетингового предложения</p>
     *
     * @return String массив значений для класса сущности {@link TRM06Entity}
     */
    private String[] getTRM06FileValue() {
        String[] strTRM06 = new String[25];

        strTRM06[0] = getId();
        strTRM06[1] = String.valueOf((int) ((Math.random() * ((9999 - 1000) + 1)) + 1000));//уник номер реш КК
        strTRM06[2] = "110000";//мин предоодобр сумма
        strTRM06[3] = "220000";//мах предоодобр сумма
        strTRM06[4] = String.valueOf((int) ((Math.random() * ((99 - 10) + 1)) + 10));//проц ставка
        strTRM06[5] = "36";//срок кредита
        strTRM06[6] = getNumberRedemptionAgreement();//номер договора для погашения
        strTRM06[7] = getSystemPayment();//система ведения погашаемого кред(для топап)
        strTRM06[8] = "6000";//ежемесячный платеж
        strTRM06[9] = getMainDecisionCreditConveyorFlag();//признак основн реш КК
        strTRM06[10] = getBisquitBranchId();//код филиала погаш договора
        strTRM06[11] = "";//тип контракта в сист-источнике
        strTRM06[12] = getCodeSubproduct(typeDecisionCreditConveyor);//код субродукта
        strTRM06[13] = getTypeDecisionCreditConveyor();//тип решения
        strTRM06[14] = getTypeDecisionCreditConveyor();//краткое описан продукта
        for (int i=15;i<25;i++)
            strTRM06[i] = "";

        return strTRM06;
    }

    /**
     * <p>Метод, генерирующий список массивов значений маркетингового предложения для класса-сущности
     * {@link TRM06Entity} с типом решения КК равным PACC</p>
     *
     * <p>Содержит шесть типов решения КК</p>
     *
     * @return String массив значений для класса сущности {@link TRM06Entity}
     */
    private List<String[]> getTRM06FileValuesPACC() {
        List<String[]> trm06ValuesPACC = new ArrayList<>();

        trm06ValuesPACC.add(getTRM06FileValuesPACC("011","N", TypeDecisionCreditConveyor.PACC,
                "24,КS,00047","PACC"));
        trm06ValuesPACC.add(getTRM06FileValuesPACC("012","N", TypeDecisionCreditConveyor.PACL,
                "15,ПХ,00002","PACL"));
        trm06ValuesPACC.add(getTRM06FileValuesPACC("013","N", TypeDecisionCreditConveyor.PACL_PACC,
                "24,КS,00047","PACC"));
        trm06ValuesPACC.add(getTRM06FileValuesPACC("014","Y", TypeDecisionCreditConveyor.PACL_PACC,
                "15,ПХ,00002","PACL"));
        trm06ValuesPACC.add(getTRM06FileValuesPACC("015","N", TypeDecisionCreditConveyor.PACL_PACC,
                "15,ПХ,00002","PACL"));
        trm06ValuesPACC.add(getTRM06FileValuesPACC("016","N", TypeDecisionCreditConveyor.PACL_PACC,
                "15,ПХ,00002","PACL"));
        return trm06ValuesPACC;
    }

    /**
     * <p>Метод осуществляет маппинг справочных значений и генерирует массив входных данных для файла,
     * соответствующего сущности {@link TRM06Entity} маркетингового предложения
     * с типом решения кредитного конвейера равным PACC</p>
     *
     * @return String массив значений для класса сущности {@link TRM06Entity}
     */
    private String[] getTRM06FileValuesPACC(String number, String mainFlag, TypeDecisionCreditConveyor typeDecisionCreditConveyor,
                                            String codeSuproduct, String microInfoProduct) {
        String[] strTRM06 = new String[25];
        strTRM06[0] = getId();
        strTRM06[1] = number;//уник номер реш КК
        strTRM06[2] = "110000";//мин предоодобр сумма
        strTRM06[3] = "220000";//мах предоодобр сумма
        strTRM06[4] = String.valueOf((int) ((Math.random() * ((99 - 10) + 1)) + 10));//проц ставка
        strTRM06[5] = "36";//срок кредита
        strTRM06[6] = getNumberRedemptionAgreement();//номер договора для погашения
        strTRM06[7] = getSystemPayment();//система ведения погашаемого кред(для топап)
        strTRM06[8] = "5000";//ежемесячный платеж
        strTRM06[9] = mainFlag;//признак основн реш КК
        strTRM06[10] = "";//код филиала погаш договора
        strTRM06[11] = "";//тип контракта в сист-источнике
        strTRM06[12] = codeSuproduct;//код субродукта
        strTRM06[13] = typeDecisionCreditConveyor.getName();//тип решения
        strTRM06[14] = microInfoProduct;//краткое описан продукта
        for (int i=15;i<25;i++)
            strTRM06[i] = "";
        return strTRM06;
    }

    /**
     * <p>Метод, генерирующий список массивов значений маркетингового предложения для класса-сущности
     * {@link TRM06Entity} для кампаний спец. вклад</p>
     *
     * <p>Содержит три типа решения КК</p>
     *
     * @return String массив значений для класса сущности {@link TRM06Entity}
     */
    private List<String[]> getTRM06FileValuesSpecDeposit() {
        List<String[]> trm06ValuesSpecDeposit = new ArrayList<>();

        trm06ValuesSpecDeposit.add(getTRM06FileValuesSpecDeposit
                ("1","N","999999,0000000"));
        trm06ValuesSpecDeposit.add(getTRM06FileValuesSpecDeposit
                ("2","N","9999999,0000000"));
        trm06ValuesSpecDeposit.add(getTRM06FileValuesSpecDeposit
                ("3","Y","10000000,0000000"));

        return trm06ValuesSpecDeposit;
    }

    /**
     * <p>Метод осуществляет маппинг справочных значений и генерирует массив входных данных для файла,
     * соответствующего сущности {@link TRM06Entity} маркетингового предложения
     * для спец. вкладов</p>
     *
     * @return String массив значений для класса сущности {@link TRM06Entity}
     */
    private String[] getTRM06FileValuesSpecDeposit(String number, String mainFlag, String maxSum) {
        String[] strTRM06 = new String[25];
        strTRM06[0] = getId();
        strTRM06[1] = number;//уник номер реш КК
        strTRM06[2] = "400000,0000000";//мин предоодобр сумма
        strTRM06[3] = maxSum;//мах предоодобр сумма
        strTRM06[4] = String.valueOf((int) ((Math.random() * ((99 - 10) + 1)) + 10));//проц ставка
        strTRM06[5] = "";//срок кредита
        strTRM06[6] = "";//номер договора для погашения
        strTRM06[7] = "";//система ведения погашаемого кред(для топап)
        strTRM06[8] = "";//ежемесячный платеж
        strTRM06[9] = mainFlag;//признак основн реш КК
        strTRM06[10] = "";//код филиала погаш договора
        strTRM06[11] = "";//тип контракта в сист-источнике
        strTRM06[12] = "";//код субродукта
        strTRM06[13] = "";//тип решения
        strTRM06[14] = "";//краткое описан продукта
        strTRM06[15] = "Вклад 1";
        strTRM06[16] = "1";
        strTRM06[17] = "3";
        strTRM06[18] = "6";
        strTRM06[19] = "400000,00";
        strTRM06[20] = "Капитализация %";
        strTRM06[21] = "Ставка для группы оттока А";
        strTRM06[22] = "RUR";
        strTRM06[23] = "N";
        strTRM06[24] = "N";
        return strTRM06;
    }

    /**
     * <p>Метод получает признак основного решения КК</p>
     * <p>Y - является основным решением </p>
     *
     * @return - признак основного решения КК
     */
    private String getMainDecisionCreditConveyorFlag() {
        if (typeDecisionCreditConveyor == TypeDecisionCreditConveyor.PACC)
            return "";
        return "Y";
    }

    /**
     * <p>Метод получает номер договора для погашения (только для заявки TOPUP)</p>
     *
     * @return - номер договора для погашения
     */
    private String getNumberRedemptionAgreement() {
        if (typeDecisionCreditConveyor == TypeDecisionCreditConveyor.TOP_UP)
            return "625/0000-0184497";
        return "";
    }

    /**
     * <p>Метод получает систему ведения погашения кредита</p>
     *
     * @return - система ведения погашения кредита
     */
    private String getSystemPayment() {
        if (typeDecisionCreditConveyor == TypeDecisionCreditConveyor.TOP_UP)
            return "99995";
        return "";
    }
}
