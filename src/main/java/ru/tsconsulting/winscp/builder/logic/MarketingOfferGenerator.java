package ru.tsconsulting.winscp.builder.logic;

import com.github.javafaker.Faker;
import ru.tsconsulting.winscp.catalog.BisquitBranch;
import ru.tsconsulting.winscp.catalog.MarketingOfferChannel;
import ru.tsconsulting.winscp.catalog.TypeCampaign;
import ru.tsconsulting.winscp.catalog.TypeDecisionCreditConveyor;

/**
 * <h3>Класс, содержащий основные опции для генерации маркетингового предложения</h3>
 *
 * @author ibetsis
 */
public class MarketingOfferGenerator {

    /**
     * <p>Поле, соответствующее id клиента в системе Siebel ФРОНТ</p>
     */
    protected static String clientId;

    /**
     * <p>Поле, соответсвующее номеру маркетингового предложения</p>
     *
     * <p>Бизнес-логика маркетингового предложния требует, чтобы оно было уникальным</p>
     */
    protected static String id;

    /**
     * <p>Поле, соответствующее названию кампании, делающей МП</p>
     */
    private static String companyName =
            Faker.instance().company().name().replaceAll("\\W", "_")
                    + String.valueOf((int) ((Math.random() * ((999 - 100) + 1)) + 100));

    /**
     * <p>Поле, соответствующее Id БИСквита</p>
     */
    private static String bisquitBranchId;

    /**
     * <p>Поле, соответствующее названию БИСквит (система-источник)</p>
     */
    private static String bisquitBranchName;

    /**
     * <p>Поле, соответствующее типу кампании</p>
     */
    private static TypeCampaign typeCampaign;

    /**
     * <p>Поле, соответствующее типам решений КК</p>
     *
     * <p>Значение берется из перечисления {@link TypeDecisionCreditConveyor}</p>
     */
    protected static TypeDecisionCreditConveyor typeDecisionCreditConveyor;

    /**
     * <p>Поле, соответствующее каналу обращения для маркетингового предложения</p>
     *
     * <p>Значение берется из перечисления {@link MarketingOfferChannel}</p>
     */
    private static MarketingOfferChannel marketingOfferChannel;


    /**
     * <p>Конструктор, генерирующий id маркетингового предложения</p>
     */
    public MarketingOfferGenerator() {
        id = String.valueOf((long)
                ((Math.random() * ((99_999_999_999_999L - 10_000_000_000_000L) + 1)) + 10_000_000_000_000L));
    }


    //SETTER------------------------------------------------------------------------------------------------------------

    /**
     * <p>Метод устанавливает тип кампании маркетингового предложения</p>
     * @param typeCampaign - объект перечисления {@link TypeCampaign}
     */
    public static void setTypeCampaign(TypeCampaign typeCampaign) {
        MarketingOfferGenerator.typeCampaign = typeCampaign;
    }

    /**
     * <p>Метод устанавливает параметры филиала БИСквита</p>
     *
     * @param bisquitBranch - объект перечисления {@link BisquitBranch}
     */
    public static void setBisquitBranch(BisquitBranch bisquitBranch) {
        MarketingOfferGenerator.bisquitBranchId = bisquitBranch.getBisquitId();
        MarketingOfferGenerator.bisquitBranchName = bisquitBranch.getBisquitName();
    }
    /**
     * Метод устанавливает приоритетный тип решения КК для маркетингово предложения
     *
     * @param typeDecisionCreditConveyor - объект перечисления типов решения КК {@link TypeDecisionCreditConveyor}
     */
    public static void setTypeDecisionCreditConveyor(TypeDecisionCreditConveyor typeDecisionCreditConveyor) {
        MarketingOfferGenerator.typeDecisionCreditConveyor = typeDecisionCreditConveyor;
    }
    /**
     * <p>Метод устанавливает канал обращения для маркетингового предложения</p>
     *
     * @param marketingOfferChannel - объект перечисления каналов обращения для МП {@link MarketingOfferChannel}
     */
    public static void setMarketingOfferChannel(MarketingOfferChannel marketingOfferChannel) {
        MarketingOfferGenerator.marketingOfferChannel = marketingOfferChannel;
    }

    /**
     * <p>Метод устанавливает id клиента в системе Siebel, которому требуется сделать маркетинговое предложение</p>
     * @param clientId
     */
    public static void setClientId(String clientId) { MarketingOfferGenerator.clientId = clientId; }
    public static void setCompanyName(String companyName) { MarketingOfferGenerator.companyName = companyName; }

    //GETTER------------------------------------------------------------------------------------------------------------

    public static String getTypeCampaign() { return typeCampaign.getTypeCampaign(); }
    public static String getBisquitBranchId() {
        return bisquitBranchId;
    }
    public static String getBisquitBranchName() { return bisquitBranchName; }
    public static String getTypeDecisionCreditConveyor() {
        return typeDecisionCreditConveyor.getName();
    }
    public static String getMarketingOfferChannel() {
        return marketingOfferChannel.toString();
    }
    public static String getClientId() {
        return clientId;
    }
    public static String getId() { return id; }
    public static String getCompanyName() { return companyName; }

    /**
     * <p>Метод получающий код субпродукта в зависимости от типа решения кредитного конвейера</p>
     *
     * @param typeDecisionCreditConveyor - объект перечисления {@link TypeDecisionCreditConveyor}
     * @return - код субпродукта
     */
    public static String getCodeSubproduct(TypeDecisionCreditConveyor typeDecisionCreditConveyor) {
        switch (typeDecisionCreditConveyor) {
            case PACL:
                return "15,ПХ,00002";
            case TOP_UP:
                return "15,ПХ,00071";
            case PACC:
                return "24,КS,00047";
            case PACL_PACC:
                return "15,ПХ,00071";
            default:
                return "";
        }
    }
}
