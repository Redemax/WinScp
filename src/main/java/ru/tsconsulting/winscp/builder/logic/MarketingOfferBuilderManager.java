package ru.tsconsulting.winscp.builder.logic;

import org.apache.commons.lang3.tuple.Pair;
import ru.tsconsulting.winscp.builder.entity.TRM01Entity;
import ru.tsconsulting.winscp.builder.entity.TRM06Entity;

import java.util.ArrayList;
import java.util.List;

/**
 * <h3>Класс-менеджер, создающий на основе {@link TRM01Builder}, {@link TRM06Builder} маркетинговое предложение</h3>
 *
 * @author ibetsis
 */
public final class MarketingOfferBuilderManager {

    /**
     * <p>Название кампании в TRM</p>
     */
    private String companyName;

    /**
     * <p>Метод, конструирующий маркетиноговое предложение для клиента</p>
     *
     * <p>Метод создает две сущности, соответсвующие {@link TRM01Entity} - загрузка данных по участникам кампании ФЛ,
     *       {@link TRM06Entity} - экспорт матрицы решений КК</p>
     *
     * @return пара объектов классов-сущностей {@link TRM01Entity}, {@link TRM06Entity}, образующих единое
     *         маркетинговое предложение. Левым в паре является {@link TRM01Entity}, правым - {@link TRM06Entity}
     *
     * @throws IllegalAccessException - ошибка доступа к полям класса
     */
    public Pair<TRM01Entity, TRM06Entity> constructorMarketingOffer() throws IllegalAccessException {
        TRM01Builder TRM01Builder = new TRM01Builder();
        TRM06Builder TRM06Builder = new TRM06Builder();

        TRM01ValuesGenerator trm01ValuesGenerator = new TRM01ValuesGenerator();
        TRM06ValuesGenerator trm06ValuesGenerator = new TRM06ValuesGenerator();

        String[] TRM01FileValues = trm01ValuesGenerator.TRM01FileValues();
        String[] TRM06FileValues = trm06ValuesGenerator.TRM06FileValues();

        companyName = TRM01FileValues[1];

        TRM01Entity TRM01Entity = TRM01Builder.getTRM01(TRM01FileValues);
        TRM06Entity TRM06Entity = TRM06Builder.getTRM06(TRM06FileValues);

        return Pair.of(TRM01Entity, TRM06Entity);
    }

    /**
     * <p>Метод, конструирующий маркетиноговое предложение с решением кредитного конвейера PACC для клиента</p>
     *
     * <p>Метод создает две сущности, соответсвующие {@link TRM01Entity} - загрузка данных по участникам кампании ФЛ,
     *       {@link TRM06Entity} - экспорт матрицы решений КК</p>
     *
     * @return пара объектов классов-сущностей {@link TRM01Entity}, {@link TRM06Entity}, образующих единое
     *         маркетинговое предложение. Левым в паре является {@link TRM01Entity}, правым - {@link TRM06Entity}
     *
     * @throws IllegalAccessException - ошибка доступа к полям класса
     */
    public Pair<TRM01Entity,List<TRM06Entity>> constructorMarketingOfferPACC() throws IllegalAccessException {
        TRM01Builder trm01Builder = new TRM01Builder();
        TRM06Builder trm06Builder = new TRM06Builder();

        TRM01ValuesGenerator trm01ValuesGenerator = new TRM01ValuesGenerator();
        TRM06ValuesGenerator trm06ValuesGenerator = new TRM06ValuesGenerator();

        String[] TRM01FileValues = trm01ValuesGenerator.TRM01FileValues();

        TRM01Entity trm01Entity = trm01Builder.getTRM01(TRM01FileValues);

        List<TRM06Entity> trm06EntityArrayList= new ArrayList<>();

        for (String[] trm06 : trm06ValuesGenerator.TRM06FileValuesPACC()) {
            trm06EntityArrayList.add(trm06Builder.getTRM06(trm06));
        }

        companyName = TRM01FileValues[1];

        return Pair.of(trm01Entity,trm06EntityArrayList);
    }

    /**
     * <p>Метод, конструирующий маркетиноговое предложение соответствующие спец вкладу</p>
     *
     * <p>Метод создает две сущности, соответсвующие {@link TRM01Entity} - загрузка данных по участникам кампании ФЛ,
     *       {@link TRM06Entity} - экспорт матрицы решений КК</p>
     *
     * @return пара объектов классов-сущностей {@link TRM01Entity}, {@link TRM06Entity}, образующих единое
     *         маркетинговое предложение. Левым в паре является {@link TRM01Entity}, правым - {@link TRM06Entity}
     *
     * @throws IllegalAccessException - ошибка доступа к полям класса
     */
    public Pair<TRM01Entity,List<TRM06Entity>> constructorMarketingOfferSpecDeposit() throws IllegalAccessException {
        TRM01Builder trm01Builder = new TRM01Builder();
        TRM06Builder trm06Builder = new TRM06Builder();

        TRM01ValuesGenerator trm01ValuesGenerator = new TRM01ValuesGenerator();
        TRM06ValuesGenerator trm06ValuesGenerator = new TRM06ValuesGenerator();

        String[] TRM01FileValues = trm01ValuesGenerator.TRM01FileValues();

        TRM01Entity trm01Entity = trm01Builder.getTRM01(TRM01FileValues);

        List<TRM06Entity> trm06EntityArrayList= new ArrayList<>();

        for (String[] trm06 : trm06ValuesGenerator.TRM06FileValuesSpecDeporsit()) {
            trm06EntityArrayList.add(trm06Builder.getTRM06(trm06));
        }

        companyName = TRM01FileValues[1];

        return Pair.of(trm01Entity,trm06EntityArrayList);
    }

    public String getCompanyName() {
        return companyName;
    }
}
