package ru.tsconsulting.winscp.builder.logic;

import ru.tsconsulting.winscp.builder.entity.TRM01Entity;

/**
 * <h3>Строитель класса {@link TRM01Entity}</h3>
 *
 * @author ibetsis
 */
public final class TRM01Builder {
    /**
     * <p>Поле, соответсвующее номеру маркетингового предложения</p>
     * <p>Бизнес-логика маркетингового предложния требует, чтобы оно было уникальным</p>
     * <p>Генерируется с помощью {@link ru.tsconsulting.winscp.builder.logic.MarketingOfferGenerator}</p>
     */
    private String id;

    /**
     * <p>Название кампании в TRM</p>
     */
    private String companyName;

    /**
     * <p>Код кампании в TRM</p>
     */
    private String companyCode;

    /**
     * <p>Тип кампании</p>
     */
    private String marketingOfferType;

    /**
     * <p>Номер предложения</p>
     */
    private String offerNumber;

    /**
     * <p>Название предложения</p>
     */
    private String offerName;

    /**
     * <p>Приоритет предложения</p>
     */
    private String offerPrior;

    /**
     * <p>Канал, по которому поступает маркетинговое предложение</p>
     */
    private String channel;

    /**
     * <p>ГРК (хз)</p>
     */
    private String grk;

    /**
     * <p>Идентификатор клиента в системе Siebel</p>
     */
    private String clientId;

    /**
     * <p>Id БИСквита</p>
     */
    private String biskvitId;

    /**
     * <p>БИСквит (система-источник)</p>
     */
    private String biskvitName;

    /**
     * <p>Номер предложения в БИСквите</p>
     */
    private String offerNumberInBC;

    /**
     * <p>Временная зона</p>
     */
    private String timeZone;

    /**
     * <p>Флаг удаления из списка (значения: 0/1)</p>
     */
    private String deleteFlag;

    /**
     * <p>Информация о маркетинговом предложении для клиента</p>
     */
    private String offerInfo;

    /**
     * <p>Дата выдачи продукта участнику кампании</p>
     */
    private String dateOutput;

    /**
     * <p>Дата заведения заявки на продукт</p>
     */
    private String startDate;

    /**
     * <p>Статус заявки на кредитный продукт (заявки с сайта, на КК и КН)</p>
     */
    private String offerStatus;

    /**
     * <p>Срок истечения предложения
     * (для TOPUP/PACL должно содержать дату истечения предодобренного предложения)</p>
     */
    private String endDate;

    /**
     * <p>Одобренная процентная ставка для PAAL, PACL, и остальных кредитов
     * (для TOPUP/PACL процентная ставка основного решения КК из матрицы решений предложения)</p>
     *
     * <p>Для Релиза 4 не заполняется</p>
     */
    private String interestRate;

    /**
     *  <p>Сумма выданного кредита, одобренный лимит по заявкам на PAAL, PACL, и сумма вклада
     *  для депозитов (для TOPUP/PACL сумма основного решения КК из матрицы решений предложения</p>
     *
     *  <p>Для Релиза 4 не заполняется</p>
     */
    private String sumOutputCredit;

    /**
     * <p>Срок кредита, одобренный срок по заявкам на PAAL, PACL, и срок вклада для депозитов
     * (для TOPUP/PACL срок основного решения КК из матрицы решений предложения)</p>
     *
     * <p>Для Релиза 4 не заполняется</p>
     */
    private String timeOutputCredit;

    /**
     * <p>Валюта, в которой предлагается продукт (для TOPUP/PACL/PACC код валюты предложения)</p>
     */
    private String currency;

    /**
     * <p>Плановая дата закрытия вклада</p>
     */
    private String endDatePlaning;

    /**
     * <p>Номер отделения, в котором была оформлена заявка</p>
     */
    private String offerDepartmentNumber;

    /**
     * <p>Наименование переменного поля 1</p>
     */
    private String nameVar1;

    /**
     * <p>Значение переменного поля 1</p>
     */
    private String valueVar1;

    /**
     * <p>Наименование переменного поля 2</p>
     */
    private String nameVar2;

    /**
     * <p>Значение переменного поля 2</p>
     */
    private String valueVar2;

    /**
     * <p>Наименование переменного поля 3</p>
     */
    private String nameVar3;

    /**
     * <p>Значение переменного поля 3</p>
     */
    private String valueVar3;

    /**
     * <p>Наименование переменного поля 4</p>
     */
    private String nameVar4;

    /**
     * <p>Значение переменного поля 4</p>
     */
    private String valueVar4;

    /**
     * <p>Наименование переменного поля 5</p>
     */
    private String nameVar5;

    /**
     * <p>Значение переменного поля 5</p>
     */
    private String valueVar5;

    /**
     * <p>Наименование переменного поля 6</p>
     */
    private String nameVar6;

    /**
     * <p>Значение переменного поля 6</p>
     */
    private String valueVar6;

    /**
     * <p>Наименование переменного поля 7</p>
     */
    private String nameVar7;

    /**
     * <p>Значение переменного поля 7</p>
     */
    private String valueVar7;

    /**
     * <p>Наименование переменного поля 8</p>
     */
    private String nameVar8;

    /**
     * <p>Значение переменного поля 8</p>
     */
    private String valueVar8;

    /**
     * <p>Наименование переменного поля 9</p>
     */
    private String nameVar9;

    /**
     * <p>Значение переменного поля 9</p>
     */
    private String valueVar9;

    /**
     * <p>Наименование переменного поля 10</p>
     */
    private String nameVar10;

    /**
     * <p>Значение переменного поля 10</p>
     */
    private String valueVar10;

    /**
     * <p>Код субпродукта (необходимо заполнять кодом субпродукта маркетингового предложения,
     * существующий параметр PRODUCT заполнять соответствующим кодом продукта</p>
     *
     * <p>поле обязательно для кампаний типа TOPUP/PACL</p>
     *
     * <p>Для Релиза 4 не заполняется</p>
     */
    private String subProduct;

    /**
     * <p>Код продукта</p>
     * <p>Для Релиза 4 не заполняется</p>
     */
    private String product;

    /**
     * <p>Флаг, указывающий на наличие матрицы решений КК для данного предложения (флаг (Y/N)
     * отражающий наличие дочерних записей матрицы решений КК в отдельном файле)</p>
     */
    private String conclusionCreditConveyor;

    /**
     * <p>Признак необходимости создать задачу, связанную с карточкой  участника МП</p>
     *
     * <p>Y – необходимо создавать задачу</p>
     */
    private String needCreateTask;

    /**
     * <p>Дата завершения план по задаче</p>
     */
    private String expireDateTask;

    /**
     * <p>Тип задачи. Обязательно в случае указания подтипа задачи</p>
     */
    private String typeTask;

    /**
     * <p>Подтип задачи. При указании подтипа задачи обязательно должен быть указан тип задачи</p>
     */
    private String subtypeTask;

    /**
     * <p>Код бисквита ТП супервизора</p>
     */
    private String codeTPSupervision;

    /**
     * <p>Приоритетный тип решения для МП (заполняется для кампаний Релиза 4)</p>
     */
    private String priorTypeForOffer;

    /**
     * <p>Признак необходимости установки отклика на МП</p>
     *
     * <p>Y – необходимо установить отклик.
     * В случае передачи пустого значения по умолчанию устанавливается значение N</p>
     */
    private String needResponse;

    /**
     * <p>Метод, создающий объект класса-сущности {@link TRM01Entity}, на основании значений полученных с помощью
     *  генератора {@link MarketingOfferGenerator} </p>
     *
     * @param TRM01FileValues - String массив, соответсвующий маркетинговому предложению. Заполняется с помощью
     *                          {@link ru.tsconsulting.winscp.builder.logic.TRM06ValuesGenerator}
     * @return - объект класса {@link TRM01Entity}
     * @throws IllegalAccessException - ошибка доступа к полям класса
     */
    TRM01Entity getTRM01(String[] TRM01FileValues) throws IllegalAccessException {
        return new TRM01Entity(TRM01FileValues);
    }

    public void setId(String id) {
        this.id = id;
    }
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }
    public void setMarketingOfferType(String marketingOfferType) {
        this.marketingOfferType = marketingOfferType;
    }
    public void setOfferNumber(String offerNumber) {
        this.offerNumber = offerNumber;
    }
    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }
    public void setOfferPrior(String offerPrior) {
        this.offerPrior = offerPrior;
    }
    public void setChannel(String channel) {
        this.channel = channel;
    }
    public void setGrk(String grk) {
        this.grk = grk;
    }
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
    public void setBiskvitId(String biskvitId) {
        this.biskvitId = biskvitId;
    }
    public void setBiskvitName(String biskvitName) {
        this.biskvitName = biskvitName;
    }
    public void setOfferNumberInBC(String offerNumberInBC) {
        this.offerNumberInBC = offerNumberInBC;
    }
    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }
    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
    public void setOfferInfo(String offerInfo) {
        this.offerInfo = offerInfo;
    }
    public void setDateOutput(String dateOutput) {
        this.dateOutput = dateOutput;
    }
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }
    public void setOfferStatus(String offerStatus) {
        this.offerStatus = offerStatus;
    }
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
    public void setInterestRate(String interestRate) {
        this.interestRate = interestRate;
    }
    public void setSumOutputCredit(String sumOutputCredit) {
        this.sumOutputCredit = sumOutputCredit;
    }
    public void setTimeOutputCredit(String timeOutputCredit) {
        this.timeOutputCredit = timeOutputCredit;
    }
    public void setCurrency(String currency) {
        this.currency = currency;
    }
    public void setEndDatePlaning(String endDatePlaning) {
        this.endDatePlaning = endDatePlaning;
    }
    public void setOfferDepartmentNumber(String offerDepartmentNumber) {
        this.offerDepartmentNumber = offerDepartmentNumber;
    }
    public void setNameVar1(String nameVar1) {
        this.nameVar1 = nameVar1;
    }
    public void setValueVar1(String valueVar1) {
        this.valueVar1 = valueVar1;
    }
    public void setNameVar2(String nameVar2) {
        this.nameVar2 = nameVar2;
    }
    public void setValueVar2(String valueVar2) {
        this.valueVar2 = valueVar2;
    }
    public void setNameVar3(String nameVar3) {
        this.nameVar3 = nameVar3;
    }
    public void setValueVar3(String valueVar3) {
        this.valueVar3 = valueVar3;
    }
    public void setNameVar4(String nameVar4) {
        this.nameVar4 = nameVar4;
    }
    public void setValueVar4(String valueVar4) {
        this.valueVar4 = valueVar4;
    }
    public void setNameVar5(String nameVar5) {
        this.nameVar5 = nameVar5;
    }
    public void setValueVar5(String valueVar5) {
        this.valueVar5 = valueVar5;
    }
    public void setNameVar6(String nameVar6) {
        this.nameVar6 = nameVar6;
    }
    public void setValueVar6(String valueVar6) {
        this.valueVar6 = valueVar6;
    }
    public void setNameVar7(String nameVar7) {
        this.nameVar7 = nameVar7;
    }
    public void setValueVar7(String valueVar7) {
        this.valueVar7 = valueVar7;
    }
    public void setNameVar8(String nameVar8) {
        this.nameVar8 = nameVar8;
    }
    public void setValueVar8(String valueVar8) {
        this.valueVar8 = valueVar8;
    }
    public void setNameVar9(String nameVar9) {
        this.nameVar9 = nameVar9;
    }
    public void setValueVar9(String valueVar9) {
        this.valueVar9 = valueVar9;
    }
    public void setNameVar10(String nameVar10) {
        this.nameVar10 = nameVar10;
    }
    public void setValueVar10(String valueVar10) {
        this.valueVar10 = valueVar10;
    }
    public void setSubProduct(String subProduct) {
        this.subProduct = subProduct;
    }
    public void setProduct(String product) {
        this.product = product;
    }
    public void setConclusionCreditConveyor(String conclusionCreditConveyor) {
        this.conclusionCreditConveyor = conclusionCreditConveyor;
    }
    public void setNeedCreateTask(String needCreateTask) {
        this.needCreateTask = needCreateTask;
    }
    public void setExpireDateTask(String expireDateTask) {
        this.expireDateTask = expireDateTask;
    }
    public void setTypeTask(String typeTask) {
        this.typeTask = typeTask;
    }
    public void setSubtypeTask(String subtypeTask) {
        this.subtypeTask = subtypeTask;
    }
    public void setCodeTPSupervision(String codeTPSupervision) {
        this.codeTPSupervision = codeTPSupervision;
    }
    public void setPriorTypeForOffer(String priorTypeForOffer) {
        this.priorTypeForOffer = priorTypeForOffer;
    }
    public void setNeedResponse(String needResponse) {
        this.needResponse = needResponse;
    }

}
