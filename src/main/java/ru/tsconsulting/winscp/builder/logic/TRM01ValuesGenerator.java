package ru.tsconsulting.winscp.builder.logic;

import ru.tsconsulting.winscp.builder.entity.TRM01Entity;
import ru.tsconsulting.winscp.catalog.TypeDecisionCreditConveyor;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * <h3>Класс-генератор, соответствующий генерации значений для заполнения сущности {@link TRM01Entity}</h3>
 * <p>Основные опции для маркетингового предложения устанавливаются в {@link MarketingOfferGenerator}</p>
 *
 * @author ibetsis
 */
final class TRM01ValuesGenerator extends MarketingOfferGenerator {

    /**
     * <p>Метод, генерирующий массив значений маркетингового предложения для класса-сущности
     * {@link TRM01Entity}</p>
     *
     * @return String массив значений для класса сущности {@link TRM01Entity}
     */
    String[] TRM01FileValues() {
        return getTRM01FileValue();
    }

    /**
     * <p>Метод осуществляет маппинг справочных значений и генерирует массив входных данных для файла,
     * соответствующего сущности {@link TRM01Entity} маркетингового предложения</p>
     *
     * @return String массив значений для класса сущности {@link TRM01Entity}
     */
    private String[] getTRM01FileValue() {
        String[] strTRM01 = new String[56];

        strTRM01[0] = getId();
        strTRM01[1] = getCompanyName();
        strTRM01[2] = String.valueOf((int) ((Math.random() * ((99999 - 10000) + 1)) + 10000));
        strTRM01[3] = getTypeCampaign();//тип кампании
        strTRM01[4] = String.valueOf((int) ((Math.random() * ((9999 - 1000) + 1)) + 1000));
        strTRM01[5] = getCompanyName() + "_" + strTRM01[4];
        strTRM01[6] = getPrioriOffer();
        strTRM01[7] = getMarketingOfferChannel();
        strTRM01[8] = getGRK();//ГРК
        strTRM01[9] = clientId;
        strTRM01[10] = getBisquitBranchId();
        strTRM01[11] = getBisquitBranchName();
        strTRM01[12] = "";//номер предложения в бисквите
        strTRM01[13] = getTimeZone();
        strTRM01[14] = "0";//флаг удаления из списка
        strTRM01[15] = getInfoAboutMarketingOffer();//инфо а МП
        strTRM01[16] = "";//дата выдачи прод участн кампании
        strTRM01[17] = "";//дата завед заяв на прод
        strTRM01[18] = "";//статус заявк на кред прод
        strTRM01[19] = getDateStarAndEndOffer()[1];
        strTRM01[20] = "";//одобреная проц ставка
        strTRM01[21] = "";//сумма выданного кред
        strTRM01[22] = getTerm();//срок кредита
        strTRM01[23] = "";//валюта в котор предлаг продукт
        strTRM01[24] = "";//планвая дата открытия вкалада
        strTRM01[25] = "";//номер отделения в котором была оформлена заявка
        strTRM01[26] = getVarFieldName1()[0];
        strTRM01[27] = getVarFieldName1()[1];
        strTRM01[28] = getVarFieldName2()[0];
        strTRM01[29] = getVarFieldName2()[1];
        strTRM01[30] = getVarFieldName3()[0];
        strTRM01[31] = getVarFieldName3()[1];
        strTRM01[32] = getVarFieldName4()[0];
        strTRM01[33] = getVarFieldName4()[1];
        strTRM01[34] = getVarFieldName5()[0];
        strTRM01[35] = getVarFieldName5()[1];
        strTRM01[36] = getVarFieldName6()[0];//"Н6";
        strTRM01[37] = getVarFieldName6()[1];//"З6";
        strTRM01[38] = getVarFieldName7()[0];//"Н7";
        strTRM01[39] = getVarFieldName7()[1];//"З7";
        strTRM01[40] = getVarFieldName8()[0];//"Н8";
        strTRM01[41] = getVarFieldName8()[1];//"З8";
        strTRM01[42] = getVarFieldName9()[0];//"Н9";
        strTRM01[43] = getVarFieldName9()[1];//"З9";
        strTRM01[44] = getVarFieldName10()[0];//"Н10";
        strTRM01[45] = getVarFieldName10()[1];//"З10";
        strTRM01[46] = "";//getCodeSubproduct(typeDecisionCreditConveyor);//код субпродукта
        strTRM01[47] = "";//код продукта
        strTRM01[48] = "Y";//флаг наличия матрицы реш КК
        strTRM01[49] = "N";//признак необх создать задачу
        strTRM01[50] = "";//дата завершения плана по задаче
        strTRM01[51] = "";//тип задачи
        strTRM01[52] = "";//подтип задачи
        strTRM01[53] = "";///код бисквит супервизора
        strTRM01[54] = getTypeDecisionCreditConveyor();//приоритетный тип решен для МП
        strTRM01[55] = needAddResponse();//признак необх установки отклика на МП

        return strTRM01;
    }

    /**
     * <p>Метод получает значение приоритета маркетингового предложения</p>
     *
     * @return - приоритет предложения
     */
    private String getPrioriOffer() {
        switch (typeDecisionCreditConveyor) {
            case TOP_UP:
            case PACC:
                return "1";
            case SPECIAL_DEPOSIT:
                return "799";
            case PACL:
            default:
                return "999";
        }
    }

    /**
     * <p>Метод получает ГРК</p>
     *
     * @return - ГРК
     */
    private String getGRK() {
        switch (typeDecisionCreditConveyor) {
            case TOP_UP:
                return "686978985";
            case SPECIAL_DEPOSIT:
                return "137841151";
            case PACL:
            default:
                return "610000002";
        }
    }

    /**
     * <p>Метод получает информацию о маркетинговом предложении</p>
     *
     * @return - информация о маркетинговом предложении
     */
    private String getInfoAboutMarketingOffer() {
        switch (typeDecisionCreditConveyor) {
            case TOP_UP:
                return "TOP-UP Вход test";
            case SPECIAL_DEPOSIT:
                return "";
            case PACL:
            default:
                return "COMBONOISE";
        }
    }

    /**
     * <p>Метод получает флаг необходимости установки отклика на маркетинговое предложение</p>
     *
     * @return - флаг необходимости установки отклика на маркетинговое предложение
     */
    private String needAddResponse() {
        switch (typeDecisionCreditConveyor) {
            case TOP_UP:
            case PACC:
                return "Y";
            case PACL:
            default:
                return "N";
        }
    }

    /**
     * <p>Метод получает срок кредита для данного МП</p>
     *
     * @return - срок кредита
     */
    private String getTerm() {
        if (typeDecisionCreditConveyor == TypeDecisionCreditConveyor.SPECIAL_DEPOSIT)
            return "";
        return "36";
    }

    private String getTimeZone() {
        if (typeDecisionCreditConveyor == TypeDecisionCreditConveyor.SPECIAL_DEPOSIT)
            return "";
        return "+00,0";
    }

    private String[] getDateStarAndEndOffer() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.add(Calendar.MONTH, -5);
        String startDate = simpleDateFormat.format(calendar.getTime());
        calendar.add(Calendar.YEAR, 1);
        String endDate = simpleDateFormat.format(calendar.getTime());//дата истечени предлож
        if (typeDecisionCreditConveyor == TypeDecisionCreditConveyor.SPECIAL_DEPOSIT)
            return new String[]{"",""};
        return new String[]{startDate,endDate};
    }

    /**
     * <p>Метод получает наименование и значение переменного поля 1</p>
     *
     * @return - массив {наименование поля, значение поля}
     */
    private String[] getVarFieldName1() {
        if (typeDecisionCreditConveyor == TypeDecisionCreditConveyor.SPECIAL_DEPOSIT)
            return new String[]{"",""};
        return new String[]{"Ежемесячный платёж","6000"};
    }

    /**
     * <p>Метод получает наименование и значение переменного поля 2</p>
     *
     * @return - массив {наименование поля, значение поля}
     */
    private String[] getVarFieldName2() {
        if (typeDecisionCreditConveyor == TypeDecisionCreditConveyor.SPECIAL_DEPOSIT)
            return new String[]{"",""};
        return new String[]{"Подключаемая страховка","Да"};
    }

    /**
     * <p>Метод получает наименование и значение переменного поля 3</p>
     *
     * @return - массив {наименование поля, значение поля}
     */
    private String[] getVarFieldName3() {
        if (typeDecisionCreditConveyor == TypeDecisionCreditConveyor.SPECIAL_DEPOSIT)
            return new String[]{"",""};
        return new String[]{"Rubber","Да"};
    }

    /**
     * <p>Метод получает наименование и значение переменного поля 4</p>
     *
     * @return - массив {наименование поля, значение поля}
     */
    private String[] getVarFieldName4() {
        if (typeDecisionCreditConveyor == TypeDecisionCreditConveyor.SPECIAL_DEPOSIT)
            return new String[]{"",""};
        return new String[]{"No","Да"};
    }

    /**
     * <p>Метод получает наименование и значение переменного поля 5</p>
     *
     * @return - массив {наименование поля, значение поля}
     */
    private String[] getVarFieldName5() {
        if (typeDecisionCreditConveyor == TypeDecisionCreditConveyor.SPECIAL_DEPOSIT)
            return new String[]{"",""};
        return new String[]{"Название 5","Значение 5"};
    }

    /**
     * <p>Метод получает наименование и значение переменного поля 6</p>
     *
     * @return - массив {наименование поля, значение поля}
     */
    private String[] getVarFieldName6() {
        return new String[]{"",""};
    }

    /**
     * <p>Метод получает наименование и значение переменного поля 7</p>
     *
     * @return - массив {наименование поля, значение поля}
     */
    private String[] getVarFieldName7() {
        return new String[]{"",""};
    }

    /**
     * <p>Метод получает наименование и значение переменного поля 8</p>
     *
     * @return - массив {наименование поля, значение поля}
     */
    private String[] getVarFieldName8() {
        return new String[]{"",""};
    }

    /**
     * <p>Метод получает наименование и значение переменного поля 9</p>
     *
     * @return - массив {наименование поля, значение поля}
     */
    private String[] getVarFieldName9() {
        return new String[]{"",""};
    }

    /**
     * <p>Метод получает наименование и значение переменного поля 10</p>
     *
     * @return - массив {наименование поля, значение поля}
     */
    private String[] getVarFieldName10() {
        if (typeDecisionCreditConveyor == TypeDecisionCreditConveyor.SPECIAL_DEPOSIT)
            return new String[] {"","1"};
        return new String[]{"",""};
    }
}
