package ru.tsconsulting.winscp.reflection;
/**
 * <h3>Наблюдаемый класс для запуска создания маркетингового предложения <h3>
 *
 * Класс принимает от метода
 * {@link ru.tsconsulting.winscp.reflection.TestToOfferAdapter#setPayload(java.lang.String, ru.tsconsulting.winscp.reflection.annotation.LoadMarketingOffer)}
 * в качестве единственного аргумента массив объектов, включающих номер контура и аргументы аннотации
 * {@link ru.tsconsulting.winscp.reflection.annotation.LoadMarketingOffer},
 * после чего помечает свой экземляр как измененный и сообщает об этом изменении экземляру наблюдателя
 * {@link ru.tsconsulting.winscp.reflection.MarketingOfferObserver}.
 *
 * @param payload - массив объектов, включающих номер контура и аргументы аннотации
 * {@link ru.tsconsulting.winscp.reflection.annotation.LoadMarketingOffer}:
 *
 *<ol>
 *  <li>clientId - id клиента в ЕФР. Для клиента с этим id будет создано МП</li>
 *  <li>CIRCUIT - номер контура, на котором запущен тестовый класс, содержащий метод с аннотацией {@link ru.tsconsulting.winscp.reflection.annotation.LoadMarketingOffer}</li>
 *  <li>numberOfMarketingOffers - количество МП для клиента</li>
 *  <li>createdConveyor - тип решения кредитного конвейера для МП</li>
 *  <li>marketingOfferChannel - канал обращения для МП</li>
 *  <li>response - результат контакта</li>
 *  <li>resultSpecification - детализация результата</li>
 *</ol>
 *
 * @author Stanislav Ushanov
 *
 * @version 0.1
 */

import org.apache.log4j.Logger;
import java.util.Observable;

public class MarketingOfferObservable extends Observable {
    private static final Logger LOGGER = Logger.getLogger(MarketingOfferObservable.class);

    /**
     * <p>Метод, принимающий параметры маркетингового предложения из адаптера
     * {@link ru.tsconsulting.winscp.reflection.TestToOfferAdapter},
     * помечает свой экземляр как измененный и уведомляет об этом экземпляр класса
     * {@link ru.tsconsulting.winscp.reflection.MarketingOfferObserver}.</p>
     *
     * @param payload - Object массив со аргументами аннотации
     */
    void sendObserverPayload(Object[] payload) {
            setChanged();
            LOGGER.info("OBSERVABLE: Observable marked as having been changed");
            notifyObservers(payload);
            LOGGER.info("OBSERVABLE: Observer notified, payload sent to update: " + payload);
    }
}
