package ru.tsconsulting.winscp.reflection;
/**
 * <h3>Класс-наблюдатель для запуска создания маркетингового предложения <h3>
 *
 * <p>Класс принимает от метода
 * {@link ru.tsconsulting.winscp.reflection.MarketingOfferObservable#notifyObservers()}
 * в качестве единственного аргумента массив объектов, включающих номер контура и параметры аннотации
 * {@link ru.tsconsulting.winscp.reflection.annotation.LoadMarketingOffer},
 * после чего проверяет, помечен ли наблюдаемый объект как измененный и в случае выполнения этого условия запускает метод
 * {@link ru.tsconsulting.winscp.reflection.MarketingOfferObserver#update(java.util.Observable, java.lang.Object)}
 * с полученным массивом в качестве аргумента</p>
 *
 * @param payload - массив объектов, включающих номер контура и параметры аннотации
 * {@link ru.tsconsulting.winscp.reflection.annotation.LoadMarketingOffer}:
 *
 *<ol>
 *  <li>clientId - id клиента в ЕФР. Для клиента с этим id будет создано МП</li>
 *  <li>CIRCUIT - номер контура, на котором запущен тестовый класс, содержащий метод с аннотацией {@link ru.tsconsulting.winscp.reflection.annotation.LoadMarketingOffer}</li>
 *  <li>numberOfMarketingOffers - количество МП для клиента</li>
 *  <li>createdConveyor - тип решения кредитного конвейера для МП</li>
 *  <li>marketingOfferChannel - канал обращения для МП</li>
 *  <li>response - результат контакта</li>
 *  <li>resultSpecification - детализация результата</li>
 *</ol>
 *
 * @author Stanislav Ushanov
 *
 * @version 0.1
 */

import org.apache.log4j.Logger;
import ru.tsconsulting.winscp.siebel.trm.MarketingOfferUtil;

import java.util.Observable;
import java.util.Observer;

public class MarketingOfferObserver implements Observer {
    private static final Logger LOGGER = Logger.getLogger(MarketingOfferObserver.class);

    /**
     * <p>Метод, принимающий параметры маркетингового предложения из экземпляра класса
     * {@link ru.tsconsulting.winscp.reflection.MarketingOfferObservable}
     * в соответствии с аргументами аннотации
     * {@link ru.tsconsulting.winscp.reflection.annotation.LoadMarketingOffer}.</p>
     *
     * @param arg - Object массив со аргументами аннотации
     */
    @Override
    public void update(Observable o, Object arg) {
        try {
            Object[] settings = (Object[]) arg;
            LOGGER.info("OBSERVER: Receiver payload from observable\n\tPayload: " + settings);

            MarketingOfferUtil marketingOfferUtil = new MarketingOfferUtil();
            marketingOfferUtil.createMarketingOffer(settings);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
