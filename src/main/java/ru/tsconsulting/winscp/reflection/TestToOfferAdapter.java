package ru.tsconsulting.winscp.reflection;
/**
 * <h3>Адаптер для запуска создания маркетингового предложения из другого пакета<h3>
 *
 * Адаптер принимает данные из другого пакета с помощью метода
 * {@link ru.tsconsulting.winscp.reflection.TestToOfferAdapter#setOffer(org.junit.runner.Description)}
 * после чего передает в экземляр наблюдаемого класса
 * {@link ru.tsconsulting.winscp.reflection.MarketingOfferObservable#addObserver(java.util.Observer)}
 * в качестве аргумента экзепляр класса-наблюдателя
 * {@link ru.tsconsulting.winscp.reflection.MarketingOfferObserver}.
 *
 * После этого из всех аннотаций над тестовым методом выбирается {@link ru.tsconsulting.winscp.reflection.annotation.LoadMarketingOffer}
 * и передается в качестве аргумента в метод
 * {@link ru.tsconsulting.winscp.reflection.TestToOfferAdapter#setPayload(ru.tsconsulting.winscp.reflection.annotation.LoadMarketingOffer)}.
 * Если среди аргументов, переданных в аннотации есть заполненное поле
 * {@link ru.tsconsulting.winscp.reflection.annotation.LoadMarketingOffer#clientId()},
 * то вызывается метод
 * {@link ru.tsconsulting.winscp.reflection.MarketingOfferObservable#sendObserverPayload(java.lang.Object[])}
 * со следущим массивом параметров в качестве аргумента:
 *
 * <ol>
 *  <li>clientId - id клиента из поля аннотации {@link ru.tsconsulting.winscp.reflection.annotation.LoadMarketingOffer#clientId()}</li>
 *  <li>CIRCUIT - номер контура, на котором запущен тестовый класс, содержащий метод с аннотацией
 *  {@link ru.tsconsulting.winscp.reflection.annotation.LoadMarketingOffer}</li>
 *  <li>numberOfMarketingOffers - количество МП для клиента</li>
 *  <li>createdConveyor - тип решения кредитного конвейера для МП</li>
 *  <li>marketingOfferChannel - канал обращения для МП</li>
 *  <li>response - результат контакта</li>
 *  <li>resultSpecification - детализация результата</li>
 * </ol>
 *
 *
 * Если среди аргументов, переданных в аннотации, нет заполненного поля
 * {@link ru.tsconsulting.winscp.reflection.annotation.LoadMarketingOffer#clientId()},
 * то адаптер принимает данные с помощью метода
 * {@link ru.tsconsulting.winscp.reflection.TestToOfferAdapter#setPayload(java.lang.String, ru.tsconsulting.winscp.reflection.annotation.LoadMarketingOffer)}
 * после чего вызывается метод
 * {@link ru.tsconsulting.winscp.reflection.MarketingOfferObservable#sendObserverPayload(java.lang.Object[])}
 * со следущим массивом параметров в качестве аргумента:
 *
 * <ol>
 *  <li>clientId - id клиента из первого параметра метода
 *  {@link ru.tsconsulting.winscp.reflection.TestToOfferAdapter#setPayload(java.lang.String, ru.tsconsulting.winscp.reflection.annotation.LoadMarketingOffer)}
 *  {@link ru.tsconsulting.winscp.reflection.TestToOfferAdapter#setPayload(java.lang.String,
 *  ru.tsconsulting.winscp.reflection.annotation.LoadMarketingOffer)}.</li>
 *  <li>CIRCUIT - номер контура, на котором запущен тестовый класс, содержащий метод с аннотацией
 *  {@link ru.tsconsulting.winscp.reflection.annotation.LoadMarketingOffer}</li>
 *  <li>numberOfMarketingOffers - количество МП для клиента</li>
 *  <li>createdConveyor - тип решения кредитного конвейера для МП</li>
 *  <li>marketingOfferChannel - канал обращения для МП</li>
 *  <li>response - результат контакта</li>
 *  <li>resultSpecification - детализация результата</li>
 * </ol>
 *
 *
 * @param payload - массив объектов, включающих номер контура и аргументы аннотации @LoadMarketingOffer:
 *
 *<ol>
 *  <li>clientId - id клиента в ЕФР. Для клиента с этим id будет создано МП.</li>
 *  <li>CIRCUIT - номер контура, на котором запущен тестовый класс, содержащий метод с аннотацией
 *  {@link ru.tsconsulting.winscp.reflection.annotation.LoadMarketingOffer}</li>
 *  <li>numberOfMarketingOffers - количество МП для клиента</li>
 *  <li>createdConveyor - тип решения кредитного конвейера для МП</li>
 *  <li>marketingOfferChannel - канал обращения для МП</li>
 *  <li>response - результат контакта</li>
 *  <li>resultSpecification - детализация результата</li>
 *</ol>
 *
 * @author Stanislav Ushanov
 *
 * @version 0.1
 */
import org.apache.log4j.Logger;
import org.junit.runner.Description;
import ru.tsconsulting.winscp.reflection.annotation.LoadMarketingOffer;

import java.lang.annotation.Annotation;

public class TestToOfferAdapter {
    private static final Logger LOGGER = Logger.getLogger(TestToOfferAdapter.class);

    /**
     * <p>Массив со всеми аннотациями метода</p>
     */
    private Annotation[] testAnnotations;

    /**
     * <p>Экземпляр класса-наблюдателя</p>
     */
    private MarketingOfferObserver marketingOfferObserver = new MarketingOfferObserver();

    /**
     * <p>Экземпляр наблюдаемого класса</p>
     */
    private MarketingOfferObservable marketingOfferObservable = new MarketingOfferObservable();

    /**
     * <p>Метод, принимающий файл Description из текущего тестового класса,
     * извлекает из него аннотации текущего метода и передает аннотацию, соответствующую типу
     * {@link ru.tsconsulting.winscp.reflection.annotation.LoadMarketingOffer}
     * в качестве аргумента в метод
     * {@link ru.tsconsulting.winscp.reflection.TestToOfferAdapter#setPayload(LoadMarketingOffer)}.</p>
     *
     * @param description - файл Description из текущего тестового класса
     */
    public void setOffer(Description description) {
        marketingOfferObservable.addObserver(marketingOfferObserver);
        try {
            testAnnotations = description.getTestClass().getDeclaredMethod(description.getMethodName()).getAnnotations();
            LOGGER.info("ADAPTER: Method annotations parsing - SUCCESS");
        } catch (NoSuchMethodException e) {
            LOGGER.fatal("Test method not found!!!", e);
        }
        for (Annotation annotation : testAnnotations) {
            if (annotation.annotationType().equals(LoadMarketingOffer.class)) {
                setPayload((LoadMarketingOffer) annotation);
                LOGGER.info("ADAPTER: Marketing offer annotation with ID selected: " + annotation);
                break;
            }
        }
    }

    /**
     * <p>Метод, принимающий id клиента из текущего тестового метода и передает его вместе с аннотацией, соответствующую типу
     * {@link ru.tsconsulting.winscp.reflection.annotation.LoadMarketingOffer}
     * в качестве аргумента в метод
     * {@link ru.tsconsulting.winscp.reflection.TestToOfferAdapter#setPayload(String, LoadMarketingOffer)}</p>
     *
     * @param id - id клиента из текущего тестового метода
     */
    public void setOffer(String id) {
        for (Annotation annotation : testAnnotations) {
            if (annotation.annotationType().equals(LoadMarketingOffer.class)) {
                setPayload(id, (LoadMarketingOffer) annotation);
                LOGGER.info("ADAPTER: Marketing offer annotation without ID selected: " + annotation);
                break;
            }
        }
    }

    /**
     * <p>Метод, принимающий из текущего тестового метода аннотацию, соответствующую типу
     * {@link ru.tsconsulting.winscp.reflection.annotation.LoadMarketingOffer}
     * извлекает из нее аргументы и формирует из них массив, который передает в качестве аргумента в метод
     * {@link ru.tsconsulting.winscp.reflection.MarketingOfferObservable#sendObserverPayload(Object[])}</p>
     *
     * @param annotation - аннотация текущего тестового метода
     */
    private void setPayload(LoadMarketingOffer annotation) {
        if (!annotation.clientId().equals("")) {
            Object[] payload = new Object[]{
                    annotation.clientId(),
                    System.getProperty("CIRCUIT"),
                    annotation.numberOfMarketingOffers(),
                    annotation.typeDecisionCreditConveyor(),
                    annotation.marketingOfferChannel(),
                    annotation.response(),
                    annotation.resultSpecification()};
            marketingOfferObservable.sendObserverPayload(payload);
            LOGGER.info("ADAPTER: Annotation fields parsing - SUCCESS\n\t" + "Payload: " + payload);
        }
    }

    /**
     * <p>Метод, принимающий из текущего тестового метода id клиента и аннотацию, соответствующую типу
     * {@link ru.tsconsulting.winscp.reflection.annotation.LoadMarketingOffer}
     * извлекает из нее аргументы и формирует из них массив, который передает в качестве аргумента в метод
     * {@link ru.tsconsulting.winscp.reflection.MarketingOfferObservable#sendObserverPayload(Object[])}</p>
     *
     * @param id - id клиента из текущего тестового метода
     * @param annotation - аннотация текущего тестового метода
     */
    private void setPayload(String id, LoadMarketingOffer annotation) {
        if (annotation.clientId().equals("")) {
            Object[] payload = new Object[]{
                    id,
                    System.getProperty("CIRCUIT"),
                    annotation.numberOfMarketingOffers(),
                    annotation.typeDecisionCreditConveyor(),
                    annotation.marketingOfferChannel(),
                    annotation.response(),
                    annotation.resultSpecification()};
            marketingOfferObservable.sendObserverPayload(payload);
            LOGGER.info("ADAPTER: Annotation fields parsing - SUCCESS\n\t" + "Payload: " + payload);
        }
    }
}
