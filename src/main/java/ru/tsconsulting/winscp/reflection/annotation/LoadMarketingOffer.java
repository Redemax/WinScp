package ru.tsconsulting.winscp.reflection.annotation;
/**
 * <h3>Аннотация для создания мартетингового предложения<h3>
 *
 * В аннотации доступны следующие параметры для создаваемого маркетингового предложения (МП):
 *
 *  @param clientId - id клиента в ЕФР. Для клиента с этим id будет создано МП
 *  @param numberOfMarketingOffers - количество МП для клиента
 *  @param createdConveyor - тип решения кредитного конвейера для МП
 *  @param marketingOfferChannel - канал обращения для МП
 *  @param response - результат контакта
 *  @param resultSpecification - детализация результата
 *
 * @required <b>При использовании аннотации без параметров или неполном списке аргументов в аннотации
 * при создании МП недостающие аргументы будут заменены следущими аргументами по умолчанию:</b>
 *
 * <ol>
 *     <li>clientId = id клиента, сформированный в методе createFL класса MainTest</li>
 *     <li>numberOfMarketingOffers = 1</li>
 *     <li>createdConveyor = PACL</li>
 *     <li>marketingOfferChannel = INCOMING_CALL</li>
 *     <li>response = "Заинтересован"</li>
 *     <li>resultSpecification = "Оформлен в ТП"</li>
 * </ol>
 *
 * @author Stanislav Ushanov
 *
 * @version 0.1
 */
import ru.tsconsulting.winscp.catalog.MarketingOfferChannel;
import ru.tsconsulting.winscp.catalog.TypeDecisionCreditConveyor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface LoadMarketingOffer {
    /**
     * <p>id клиента в ЕФР</p>
     */
    String clientId() default "";

    /**
     * <p>Количество МП для клиента</p>
     */
    int numberOfMarketingOffers() default 1;

    /**
     * <p>Тип решения кредитного конвейера для МП</p>
     */
    TypeDecisionCreditConveyor typeDecisionCreditConveyor() default TypeDecisionCreditConveyor.PACL;

    /**
     * <p>Канал обращения для МП</p>
     */
    MarketingOfferChannel marketingOfferChannel() default MarketingOfferChannel.INCOMING_CALL;

    /**
     * <p>Результат контакта</p>
     */
    String response() default "Заинтересован";

    /**
     * <p>Детализация результата</p>
     */
    String resultSpecification() default "Оформлен в ТП";
}
