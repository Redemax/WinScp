package ru.tsconsulting.winscp.reflection.meta;

/**
 * <h3>Вспомогательный класс, хранящий информацию о контуре</h3>
 *
 * @author ibetsis
 */
public final class Circuit {

    /**
     * <p>Номер контура</p>
     * <p>Примеры значений: k3, k4, k6</p>
     */
    private static String circuit;

    public static String getCircuit() {
        return circuit;
    }

    public static void setCircuit(String circuit) {
        Circuit.circuit = circuit;
    }
}
