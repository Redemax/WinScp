package ru.tsconsulting.winscp.dataBase.service;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import ru.tsconsulting.winscp.dataBase.util.SessionUtil;
import ru.tsconsulting.winscp.dataBase.entity.SystemPref;

import java.util.List;

/**
 * <h3>Класс создан для осуществления реляционных операций с сущностью SystemPref(табл. S_SYS_PREF)</h3>
 *
 * @author ibetsis
 * @see SystemPref - класс сущности, с которой работает сервис
 */
public final class SystemPrefService extends SessionUtil {
    private static final Logger LOGGER = Logger.getLogger(SystemPrefService.class);

    /**
     * <p>Метод, получающий значение системного параметра по его имени</p>
     * <p>В случае работы с маркетинговыми предложениями, получает путь интеграционного каталога в WinSCP,
     * куда выкладываются файлы из TRM для загрузки.</p>
     *
     * @param nameSystemPref - имя системного параметра
     * @return - значение системного параметра
     */
    public String getSystemDir(String nameSystemPref) {
        String systemDir = null;
        String id = "";
        try {
            Session session = openTransactionSession();

            Criteria criteria = session.createCriteria(SystemPref.class).
                    add(Restrictions.eq("sysName", nameSystemPref));

            List<SystemPref> systemPrefList = criteria.list();

            for (SystemPref systemPref : systemPrefList) {
                id = systemPref.getId();
                break;
            }

            SystemPref systemPref = session.load(SystemPref.class, id);

            systemDir = systemPref.getDirectory();
        } catch (Exception e) {
            rollbackTransaction();
            LOGGER.error(e.getMessage(), e);
        } finally {
            closeSessionQuietly();
        }
        return systemDir;
    }

//    private String getIdSystemPref(String nameSystemPref) {
//        String id = null;
//        try {
//            Session session = openTransactionSession();
//            Criteria criteria = session.createCriteria(SystemPref.class).
//                    add(Restrictions.eq("sysName",nameSystemPref));
//
//            List<SystemPref> systemPrefList = criteria.list();
//
//            for(SystemPref systemPref : systemPrefList) {
//                id = systemPref.getId();
//                break;
//            }
//        } catch (Exception e) {
//            rollbackTransaction();
//            LOGGER.error(e.getMessage(),e);
//        } finally {
//            closeSessionQuietly();
//        }
//        return id;
//    }
}
