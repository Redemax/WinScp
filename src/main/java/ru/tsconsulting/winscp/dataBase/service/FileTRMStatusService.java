package ru.tsconsulting.winscp.dataBase.service;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import ru.tsconsulting.winscp.dataBase.entity.FileTRMStatus;
import ru.tsconsulting.winscp.dataBase.util.SessionUtil;

import java.util.List;

/**
 * <h3>Класс создан для осуществления реляционных операций с сущностью FileTRMStatus(табл. CX_TRM_FILENOT)</h3>
 * @see FileTRMStatus - класс сущности, с которой работает сервис
 *
 * @author ibetsis
 */
public final class FileTRMStatusService extends SessionUtil {
    private static final Logger LOGGER = Logger.getLogger(FileTRMStatusService.class);

    private static Integer priority;
    /**
     * <p>Метод изменяет статус и имя уже существующего (загруженного из TRM) csv файла</p>
     *
     * @param integrationPoint - точка интеграции, для поиска соответствующего файла
     * @param fileName - имя файла, <b>на которое</b> мы хотим поменять
     * @return - id (row_id) измененного файла в таблице CX_TRM_FILENOT
     */
    public String changeTRMFileNameAndStatus(String integrationPoint, String fileName) {
        String id = "";
        try {
            Session session = openTransactionSession();

            Criteria criteria =  session.createCriteria(FileTRMStatus.class).
                    add(Restrictions.eq("integrationPoint", integrationPoint)).
                    add(Restrictions.eq("state", "Complete")).
                    add(Restrictions.eq("priority",priority));
                    //addOrder(Order.desc("created"));

            List<FileTRMStatus> trmFilesStatus = criteria.list();

            for (FileTRMStatus fileTRMStatus : trmFilesStatus) {
                id = fileTRMStatus.getId();
                break;
            }

            FileTRMStatus fileTRMStatus = session.load(FileTRMStatus.class, id);
            fileTRMStatus.setFileName(fileName);
            fileTRMStatus.setState("New");

            session.update(fileTRMStatus);
        } catch (Exception e) {
            rollbackTransaction();
            LOGGER.error(e.getMessage(), e);
        } finally {
            closeSessionQuietly();
        }
        return id;
    }

    /**
     * <p>Метод, получающий статус файла полученного из TRM</p>
     *
     * @param id - id файла (row_id в таблице CX_TRM_FILENOT)
     * @return - статус файла
     */
    public String getStatusTRMLoad(String id) {
        String state = null;

        try {
            Session session = openTransactionSession();

            FileTRMStatus fileTRMStatus = session.load(FileTRMStatus.class,id);
            state = fileTRMStatus.getState();
        } catch (Exception e) {
            rollbackTransaction();
            LOGGER.error(e.getMessage(),e);
        } finally {
            closeSessionQuietly();
        }

        return state;
    }

    public static Integer getPriority() { return priority; }
    public static void setPriority(Integer priority) { FileTRMStatusService.priority = priority; }

    //    private String getIdTRMFile(String integrationPoint) {
//        String id = null;
//        try {
//            Session session = openTransactionSession();
//            Criteria criteria = session.createCriteria(FileTRMStatus.class).
//                    add(Restrictions.eq("integrationPoint", integrationPoint)).
//                    add(Restrictions.eq("state", "Complete")).
//                    addOrder(Order.desc("created"));
//
//            List<FileTRMStatus> trmFilesStatus = criteria.list();
//
//            for (FileTRMStatus fileTRMStatus : trmFilesStatus) {
//                id = fileTRMStatus.getId();
//                break;
//            }
//        } catch (Exception e) {
//            rollbackTransaction();
//            LOGGER.error(e.getMessage(), e);
//        } finally {
//            closeSessionQuietly();
//        }
//        return id;
//    }
}
