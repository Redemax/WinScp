package ru.tsconsulting.winscp.dataBase.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.tsconsulting.winscp.reflection.meta.Circuit;

/**
 * <h3>Системный класс, отвечающий за инициализацию фабрики Сессий {@link org.hibernate.Session}</h3>
 *
 * <p>Класс иницииализирует фабрику из конфигурационных файлов</p>
 *
 * @author ibetsis
 * @author mfufaev
 */
public class SessionFactoryUtil {
    private static SessionFactory sessionFactoryTRM = buildSessionFactory();

    /**
     * <p>Системный метод, инициализирующий фабрику сессий</p>
     */
    private static SessionFactory buildSessionFactory() {
        try {
            return new Configuration().configure("hibernateTRM.cfg." + Circuit.getCircuit() + ".xml")
                    .buildSessionFactory();
        }
        catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    /**
     * <p>Системный метод, закрывающих текущую фабрику сессий</p>
     */
    public static void closeSessionFactory() {
        if(!sessionFactoryTRM.isClosed())
            sessionFactoryTRM.close();
    }

    public static SessionFactory getSessionFactoryTRM() {
        return sessionFactoryTRM;
    }
}
