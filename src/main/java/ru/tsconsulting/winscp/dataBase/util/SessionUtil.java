package ru.tsconsulting.winscp.dataBase.util;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;

/**
 * <h3>Системный класс, отвечающий за создание и дальнейшую работу с сессиями и транзакциями</h3>
 *
 * <p>Точкой входа для данного класса является системный класс HibernateFactory</p>
 * @see SessionFactoryUtil - класс сущности, с которой работает сервис
 *
 * @author ibetsis
 * @author mfufaev
 */
public class SessionUtil {

    /**
     * <p>Поле, отвечающее за текущую сессию, с которой будут работать сервисы </p>
     */
    private Session session;

    /**
     * <p>Поле, отвечающее за транзакцию, которая привязана к текущей сессии</p>
     * @see #session
     */
    private Transaction transaction;

    public Session getSession() {
        return session;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    /**
     * Метод создает <b>новый объект</b> сессии, которая работает с банковским контуром с помощью фабричного метода
     * из класса HibernateFactory
     *
     * @see SessionFactoryUtil
     */
    private Session openSession() {
        return SessionFactoryUtil.getSessionFactoryTRM().openSession();
    }

    /**
     * Метод создает <b>новый объект</b> сессии, которая работает с УСБС с помощью фабричного метода из класса
     * HibernateUtil
     *
     * @see SessionFactoryUtil
     */
//    public Session openInstanceSession() {
//        return HibernateFactory.getSessionFactoryForInstanceId().openSession();
//    }

    /**
     * <p>Метод создает <b>новый объект</b> сессии с помощью фабричного метода из класса HibernateUtil и
     * активирует транзакцию, привязанную к ней (= TransactionStatus.ACTIVE)</p>
     * @see TransactionStatus#ACTIVE
     * @see SessionFactoryUtil
     */
    public Session openTransactionSession() {
        session = openSession();
        transaction = session.beginTransaction();
        return session;
    }

//    public Session openTransactionInstanceSession() {
//        session = openInstanceSession();
//        transaction = session.beginTransaction();
//        return session;
//    }

    /**
     * <p>Метод откатывает любые действия, сделанные в рамках транзакции</p>
     * <p><b>Рекомендуется использовать в блоке catch</b></p>
     *
     * При успешном выполнении метода транзакция переходит в статус TransactionStatus.ROLLED_BACK
     * @see TransactionStatus#ROLLED_BACK
     *
     * В случае ошибки отката (вероятнее всего транзакция перейдет в статус TransactionStatus.ROLLING_BACK)
     * @see TransactionStatus#ROLLING_BACK
     */
    public void rollbackTransaction() {
        if (session != null) {
            if (session.isOpen()) {
                if (transaction != null)
                    if (transaction.getStatus().canRollback())
                        transaction.rollback();
            }
        }
    }

    /**
     * Метод закрывает сессию <b>(если она открыта)</b> и
     * транзакцию, привязанную к сессии <b>(если транзакция активна)</b>
     */
    public void closeSessionQuietly() {
        if (session != null) {
            if (session.isOpen()) {
                if (transaction != null) {
                    if (transaction.getStatus().isOneOf(TransactionStatus.ACTIVE)) {
                        transaction.commit();
                        closeSession();
                    } else closeSession();
                } else closeSession();
            }
        }
    }

    /**
     * <p>Служебный метод, использующийся в {@link #closeSessionQuietly}</p>
     */
    private void closeSession() {
        session.flush();
        session.clear();

        session.close();
    }

}
