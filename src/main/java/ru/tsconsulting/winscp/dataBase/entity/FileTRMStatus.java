package ru.tsconsulting.winscp.dataBase.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * <h3>Класс-сущность соответствующий таблице CX_TRM_FILENOT</h3>
 * <p>Таблица содержит статусы файлов TRM, загружающихся в siebel CRM</p>
 *
 * @see ru.tsconsulting.winscp.dataBase.service.FileTRMStatusService - класс-проводник реляционных операций с данной сущностью
 *
 * @author ibetsis
 */
@Entity
@Table(name = "cx_trm_filenot")
public final class FileTRMStatus implements Serializable {

    /**
     * <p>Поле, соответствующее первичному ключу в таблице CX_TRM_FILENOT</p>
     * <p>Hibernate требует, чтобы аннотацией Id помечался <b>ТОЛЬКО ПЕРВИЧНЫЙ КЛЮЧ</b> таблицы</p>
     */
    @Id
    @Column(name = "row_id", length = 100)
    private String id;

    /**
     * <p>Поле, ооответствующее имени файла</p>
     */
    @Column(name = "file_name")
    private String fileName;

    /**
     * <p>Поле, соответствующее статусу загрузки файла </p>
     */
    @Column(name = "state")
    private String state;

    /**
     * <p>Поле, соответствующее типу интеграционной точки</p>
     */
    @Column(name = "int_point")
    private String integrationPoint;

    /**
     * <p>Поле, соответсвующее приоритету выполнения интеграции</p>
     */
    @Column(name = "priority")
    private Integer priority;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }

    public String getIntegrationPoint() {
        return integrationPoint;
    }
    public void setIntegrationPoint(String integrationPoint) {
        this.integrationPoint = integrationPoint;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }
}
