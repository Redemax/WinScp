package ru.tsconsulting.winscp.dataBase.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <h3>Класс-сущность соответствующий таблице S_SYS_PREF</h3>
 * <p>Таблица содержит системные настройки</p>
 *
 * @see ru.tsconsulting.winscp.dataBase.service.SystemPrefService - класс-проводник реляционных операций с данной сущностью
 *
 * @author ibetsis
 */
@Entity
@Table(name = "s_sys_pref")
public final class SystemPref {

    /**
     * <p>Поле, соответствующее первичному ключу в таблице S_SYS_PREF</p>
     * <p>Hibernate требует, чтобы аннотацией Id помечался <b>ТОЛЬКО ПЕРВИЧНЫЙ КЛЮЧ</b> таблицы</p>
     */
    @Id
    @Column(name = "row_id", length = 100)
    private String id;

    /**
     * <p>Имя системного параметра</p>
     */
    @Column(name = "sys_pref_cd")
    private String sysName;

    /**
     * <p>Значение системного параметра</p>
     */
    @Column(name = "val")
    private String directory;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getSysName() {
        return sysName;
    }
    public void setSysName(String sysName) {
        this.sysName = sysName;
    }

    public String getDirectory() {
        return directory;
    }
    public void setDirectory(String directory) {
        this.directory = directory;
    }
}
