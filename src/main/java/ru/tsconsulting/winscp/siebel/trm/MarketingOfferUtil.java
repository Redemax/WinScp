package ru.tsconsulting.winscp.siebel.trm;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import org.apache.log4j.Logger;
import ru.tsconsulting.winscp.builder.logic.MarketingOfferGenerator;
import ru.tsconsulting.winscp.catalog.BisquitBranch;
import ru.tsconsulting.winscp.catalog.MarketingOfferChannel;
import ru.tsconsulting.winscp.catalog.TypeCampaign;
import ru.tsconsulting.winscp.catalog.TypeDecisionCreditConveyor;
import ru.tsconsulting.winscp.csv.impl.CSVFileMarketingOfferCreator;
import ru.tsconsulting.winscp.dataBase.service.FileTRMStatusService;
import ru.tsconsulting.winscp.reflection.meta.Circuit;
import ru.tsconsulting.winscp.sftp.impl.SenderMarketingOffer;

import java.io.IOException;

/**
 * <h3>Main класс, отвечающий за первую часть формирования маркетиногового предложния (с формирования csv
 *          файла до загрузки его в систему Siebel Фронт)</h3>
 *
 * @author ibetsis
 */
public final class MarketingOfferUtil {
    private static final Logger LOGGER = Logger.getLogger(MarketingOfferUtil.class);

    /**
     * <p>Название кампании в TRM</p>
     */
    private static String companyName;

    /**
     * <p>id заявки в базе Siebel(столбец row_id в таблице CX_TRM_FILENOT) с точкой интеграции TRM01</p>
     */
    private static String idTRM01File;

    /**
     * <p>id заявки в базе Siebel(столбец row_id в таблице CX_TRM_FILENOT) с точкой интеграции TRM06</p>
     */
    private static String idTRM06File;

    /**
     * <p>Метод, отвечающий создание маркетиногового предложения в TRM и загрузки данных в точках интеграции
     *      TRM01 и TRM06 в Siebel Фронт</p>
     *
     * @param settings - Object массив, содержащий значения пришедшие из вызываемой программы
     *                 Формируется с помощью {@link java.util.Observer}
     * Параметры массива:
     *
     * <ol>
     *     <li>id клиента в системе ФРОНТ</li>
     *     <li>контур</li>
     *     <li>кол-во маркетинговых предложений</li>
     *     <li>тип решения КК для МП</li>
     *     <li>канал обращения для МП</li>
     * </ol>
     *
     * @throws IllegalAccessException
     * @throws IOException
     * @throws JSchException
     * @throws SftpException
     */
    public void createMarketingOffer(Object[] settings) throws IllegalAccessException, IOException, JSchException, SftpException {
        SenderMarketingOffer senderMarketingOffer = getSenderWithParametersMarketingOffer(settings);

        senderMarketingOffer.sendFileToWinSCP(Circuit.getCircuit());

        companyName = senderMarketingOffer.getCompanyName();

        FileTRMStatusService fileTRMStatusService = new FileTRMStatusService();
        idTRM01File = fileTRMStatusService.changeTRMFileNameAndStatus("TRM01", senderMarketingOffer.getTRMsFileNames()[0]);
        FileTRMStatusService.setPriority(1);
        idTRM06File = fileTRMStatusService.changeTRMFileNameAndStatus("TRM06", senderMarketingOffer.getTRMsFileNames()[1]);

        LOGGER.info("ID1=" + idTRM01File + ". ID2=" + idTRM06File);
        //SessionFactoryUtil.closeSessionFactory();
    }

    /**
     * <p>Метод, устанавливающий опции маркетингового предложения, в соответствии со значениями пришедшими из
     * аннотации {@link ru.tsconsulting.winscp.reflection.annotation.LoadMarketingOffer}</p>
     *
     * @param settings - Object массив со значениями параметров аннотации
     * @return объект класса Sender {@link SenderMarketingOffer}
     */
    private SenderMarketingOffer getSenderWithParametersMarketingOffer(Object[] settings) {
        Circuit.setCircuit((String) settings[1]);
        FileTRMStatusService.setPriority(999);
        MarketingOfferGenerator.setClientId((String) settings[0]);
        MarketingOfferGenerator.setTypeDecisionCreditConveyor((TypeDecisionCreditConveyor) settings[3]);

        if(settings[3] == TypeDecisionCreditConveyor.SPECIAL_DEPOSIT)
            MarketingOfferGenerator.setBisquitBranch(BisquitBranch.OFFICE_FOR_SPECIAL_DEPOSIT);
        else
            MarketingOfferGenerator.setBisquitBranch(BisquitBranch.HEAD_OFFICE);

        if(settings[3] == TypeDecisionCreditConveyor.TOP_UP)
            MarketingOfferGenerator.setTypeCampaign(TypeCampaign.TOP_UP);
        else if (settings[3] == TypeDecisionCreditConveyor.SPECIAL_DEPOSIT)
            MarketingOfferGenerator.setTypeCampaign(TypeCampaign.SPECIAL_DEPOSIT);
        else
            MarketingOfferGenerator.setTypeCampaign(TypeCampaign.COMBO);
        MarketingOfferGenerator.setMarketingOfferChannel((MarketingOfferChannel) settings[4]);
        CSVFileMarketingOfferCreator.setNumberOfMarketingOffers((int) settings[2]);

        SenderMarketingOffer senderMarketingOffer = new SenderMarketingOffer();
        senderMarketingOffer.setCircuitWinScp(Circuit.getCircuit());

        return senderMarketingOffer;
    }

    public static String getCompanyName() {
        return companyName;
    }
    public static String getIdTRM01File() {
        return idTRM01File;
    }
    public static String getIdTRM06File() {
        return idTRM06File;
    }
}
