package ru.tsconsulting.winscp.sftp;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

import java.io.IOException;

/**
 * <h3>Интерфейс, определяющий отправку файлов в WinSCP</h3>
 *
 * @author ibetsis
 */
public interface Sender {

    /**
     * <p>Метод, определяющий процесс отправки файлов в WinSCP</p>
     *
     * @param circuit - номер контура
     *
     * @throws IllegalAccessException
     * @throws IOException
     * @throws JSchException
     * @throws SftpException
     */
    void sendFileToWinSCP(String circuit) throws IllegalAccessException, IOException, JSchException, SftpException;
}
