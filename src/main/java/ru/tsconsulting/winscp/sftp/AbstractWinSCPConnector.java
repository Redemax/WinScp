package ru.tsconsulting.winscp.sftp;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;

/**
 * <h3>Абстракный класс для подключения к WinSCP</h3>
 *
 * @author ibetsis
 */
public abstract class AbstractWinSCPConnector {
    /**
     * <p>Абстрактный метод, который должен содержать способ установки соединения c WinSCP по SFTP протоколу</p>
     *
     * @param host - url сервера WinSCP
     * @param login - логин пользователя в WinSCP {@link ru.tsconsulting.winscp.sftp.impl.WinSCPServerConfig}
     * @param password - пароль пользователся в WinSCP {@link ru.tsconsulting.winscp.sftp.impl.WinSCPServerConfig}
     * @return - открытый SFTP канал с WinSCP
     * @throws JSchException
     */
    abstract protected ChannelSftp connectToWinSCP(String host, String login, String password) throws JSchException;

    /**
     * <p>Абстрактный метод, который должен содержать способ отключения от сервера WinSCP</p>
     *
     * @param channelSftp - открытый SFTP канал с WinSCP
     */
    abstract protected void disconnectWinSCP(ChannelSftp channelSftp);
}
