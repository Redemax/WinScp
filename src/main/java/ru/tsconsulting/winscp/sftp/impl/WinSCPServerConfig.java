package ru.tsconsulting.winscp.sftp.impl;

/**
 * <h3>Перечисление, содержащие конфигурационные настройки WinSCP сервера</h3>
 *
 * @author ibetsis
 */
public enum WinSCPServerConfig {
    K3_APP_SERVER("siebel","siebel"),
    K4_APP_SERVER("siebel","MSiHdd1m"),
    K6_APP_SERVER("siebel","siebel");

    private String login;
    private String password;

    WinSCPServerConfig(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }
    public String getPassword() {
        return password;
    }
}
