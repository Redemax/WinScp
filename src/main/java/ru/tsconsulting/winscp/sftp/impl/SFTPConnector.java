package ru.tsconsulting.winscp.sftp.impl;

import com.jcraft.jsch.*;
import org.apache.log4j.Logger;
import ru.tsconsulting.winscp.sftp.AbstractWinSCPConnector;

/**
 * <h3>Класс-коннектор к WinSCP по протоколу SFTP</h3>
 *
 * @author ibetsis
 */
public class SFTPConnector extends AbstractWinSCPConnector {
    private static final Logger LOGGER = Logger.getLogger(SFTPConnector.class);
    private JSch jSch = new JSch();
    Session session = null;

    /**
     * <p>Метод, переопределяющий абстрактный метод {@link AbstractWinSCPConnector#connectToWinSCP(String, String, String)}</p>
     *
     * <p>Соединение осуществляется с помощью создания session с WinSCP. У одной сессии может быть открыто
     * несколько каналов различных типов</p>
     *
     * @param host     - url сервера WinSCP
     * @param login    - логин пользователя в WinSCP {@link ru.tsconsulting.winscp.sftp.impl.WinSCPServerConfig}
     * @param password - пароль пользователся в WinSCP {@link ru.tsconsulting.winscp.sftp.impl.WinSCPServerConfig}
     * @return - открытый SFTP канал с WinSCP
     * @throws JSchException
     */
    @Override
    protected ChannelSftp connectToWinSCP(String host, String login, String password) throws JSchException {
        session = jSch.getSession(login, host);
        session.setPassword(password);

        java.util.Properties config = new java.util.Properties();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);
        session.connect();
        LOGGER.info("Connect to server: " + host + "with login: " + login + ", password: " + password);

        Channel channel = session.openChannel("sftp");
        channel.connect();

        ChannelSftp channelSftp = (ChannelSftp) channel;
        LOGGER.info("Create sftp channel");

        return channelSftp;
    }

    /**
     * <p>Метод, переопределяющий абстрактный метод {@link AbstractWinSCPConnector#disconnectWinSCP(ChannelSftp)}</p>
     * <p>Закрывает открытый канал и сессию в рамках которой он был открыт</p>
     *
     * @param channelSftp - открытый SFTP канал с WinSCP
     */
    @Override
    protected void disconnectWinSCP(ChannelSftp channelSftp) {
        channelSftp.exit();
        session.disconnect();
        LOGGER.info("Disconnect sftp channel: " + channelSftp.toString() + ". Disconnect session: "
                + session.getHost());
    }
}
