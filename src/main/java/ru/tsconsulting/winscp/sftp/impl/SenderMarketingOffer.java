package ru.tsconsulting.winscp.sftp.impl;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import org.apache.log4j.Logger;
import ru.tsconsulting.winscp.builder.logic.MarketingOfferGenerator;
import ru.tsconsulting.winscp.csv.impl.CSVFileMarketingOfferCreator;
import ru.tsconsulting.winscp.dataBase.service.SystemPrefService;
import ru.tsconsulting.winscp.sftp.Sender;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * <h3>Класс, предназначенный для инкапсуляции логики отправления маркетиногового предложения по протоколу SFTP
 *  с помощью {@link SFTPConnector} на WinSCP</h3>
 *
 * @author ibetsis
 */
public final class SenderMarketingOffer extends SFTPConnector implements Sender {
    private static final Logger LOGGER = Logger.getLogger(SenderMarketingOffer.class);

    /**
     * <p>Поле, соответствующее номеру контура</p>
     */
    private String circuitWinScp;

    /**
     * <p>Поле, содержащее имена отправляемых TRM файлов</p>
     */
    private String[] TRMsFileNames;

    /**
     * <p>Название кампании в TRM</p>
     */
    private String companyName;

    /**
     * <p>Метод, отправляющий файлы сформированные {@link ru.tsconsulting.winscp.builder.logic.MarketingOfferBuilderManager}
     *     по SFTP протоколу в WinSCP. Файлы соответствуют маркетинговому предложению для клиента ФЛ и точкам интеграции
     *     TRM01, TRM06</p>
     * <p>Для отправки по SFTP используется библиотека {@link com.jcraft.jsch}</p>
     *
     * @param circuit - номер контура
     * @throws IllegalAccessException
     * @throws IOException
     * @throws JSchException
     * @throws SftpException
     */
    public void sendFileToWinSCP(String circuit)
            throws IllegalAccessException, IOException, JSchException, SftpException {
        WinSCPServerConfig winSCPServerConfig = choiceWinSCPAppServer(circuit);

        CSVFileMarketingOfferCreator csvFileMarketingOfferCreator = new CSVFileMarketingOfferCreator();
        if(MarketingOfferGenerator.getTypeDecisionCreditConveyor().equals("PACC"))
            TRMsFileNames = csvFileMarketingOfferCreator.createCSVFilePACC();
        else if(MarketingOfferGenerator.getTypeDecisionCreditConveyor().equals("specdep"))
            TRMsFileNames = csvFileMarketingOfferCreator.createCSVFileSpecDeposit();
        else
            TRMsFileNames = csvFileMarketingOfferCreator.createCSVFile();

        companyName = csvFileMarketingOfferCreator.getCompanyName();

        SystemPrefService systemPrefService = new SystemPrefService();
        String pathToDir = systemPrefService.getSystemDir("VTB24 TRM Work Dir");

        SFTPConnector connector = new SFTPConnector();
        ChannelSftp channelSftp = null;

        try {
            channelSftp = connector.connectToWinSCP(getHost(circuitWinScp),
                    winSCPServerConfig.getLogin(), winSCPServerConfig.getPassword());

            try (InputStream inputStreamTRM01 = new FileInputStream(TRMsFileNames[0]);
                 InputStream inputStreamTRM06 = new FileInputStream(TRMsFileNames[1])) {
                changeWorkingDir(channelSftp, getPathToDirTRMIn(pathToDir, "1"));
                storeFile(channelSftp, TRMsFileNames[0], inputStreamTRM01);

                changeWorkingDir(channelSftp, getPathToDirTRMIn(pathToDir, "6"));
                storeFile(channelSftp, TRMsFileNames[1], inputStreamTRM06);
            }
        } finally {
            if(channelSftp != null)
                connector.disconnectWinSCP(channelSftp);
        }

        csvFileMarketingOfferCreator.deleteLocalFileMarketingOffer(TRMsFileNames[0], TRMsFileNames[1]);
    }

    /**
     * <p>Метод, получающий url хоста WinSCP для выбранного контура</p>
     *
     * @param circuitWinScp - нужный контур
     * @return - url WinSCP сервера
     */
    private String getHost(String circuitWinScp) {
        return circuitWinScp.toLowerCase() + "siebelapp.vtb24.ru";
    }

    /**
     * <p>Метод, отправляющий файл по SFTP протоколу</p>
     *
     * @param channelSftp - канал открытого SFTP соединения
     * @param fileName - имя отправляемого файла
     * @param inputStream - объект класса {@link FileInputStream}
     * @throws SftpException
     */
    private void storeFile(ChannelSftp channelSftp, String fileName, InputStream inputStream) throws SftpException {
        channelSftp.put(inputStream, fileName);
        LOGGER.info("File: " + fileName + " shipped to " + channelSftp.pwd());
    }

    /**
     * <p>Метод, изменяющий рабочую директорию</p>
     *
     * @param channelSftp - канал открытого SFTP соединения
     * @param pathToWorkingDir - путь до каталога, на который мы хотим поменять рабочую директорию
     * @throws SftpException
     */
    private void changeWorkingDir(ChannelSftp channelSftp, String pathToWorkingDir) throws SftpException {
        channelSftp.cd(pathToWorkingDir);
        LOGGER.info("Working directory changed to: " + pathToWorkingDir);
    }

    /**
     * <p>Метод, возвращающий конфигурационные настройки подключения к WinSCP для выбранного контура. Содержит логин
     *  пользователя и пароль</p>
     *
     * @param circuit - нужный контур
     * @return - объект перечисления {@link WinSCPServerConfig}
     */
    private WinSCPServerConfig choiceWinSCPAppServer(String circuit) {
        if (circuit.toLowerCase().equals("k3"))
            return WinSCPServerConfig.K3_APP_SERVER;
        else if (circuit.toLowerCase().equals("k4"))
            return WinSCPServerConfig.K4_APP_SERVER;
        else if (circuit.toLowerCase().equals("k6"))
            return WinSCPServerConfig.K6_APP_SERVER;
        else
            return null;
    }

    /**
     * <p>Метод, возвращающий полный путь в WinSCP для конкретной точки интеграции</p>
     *
     * @param path - путь до корневой папки интеграционного каталога. Получается из таблицы
     *          {@link ru.tsconsulting.winscp.dataBase.entity.SystemPref} с помощью метода
     *          {@link SystemPrefService#getSystemDir(String workDir)}
     *
     * @param numberCSVTRMFile - номер точки интеграции
     *
     * @return - путь до заданой точки интеграции
     */
    private String getPathToDirTRMIn(String path, String numberCSVTRMFile) {
        return path + "/TRM0" + numberCSVTRMFile + "/in";
    }

    public String getCompanyName() {
        return companyName;
    }

    public String[] getTRMsFileNames() {
        return TRMsFileNames;
    }

    public void setCircuitWinScp(String circuitWinScp) {
        this.circuitWinScp = circuitWinScp;
    }
}
